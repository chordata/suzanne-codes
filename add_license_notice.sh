#!/bin/bash


FILES=$(find addons/suzanne_codes -type f -name "*.py" \
		-not -path "*/__pycache__/*" \
		)

NOTICE=$(cat _license_notice.txt)

for FILE in $FILES ; do
	cp $FILE $FILE.tmp
	cat _license_notice.txt $FILE.tmp > $FILE
	rm $FILE.tmp
	
done

