import bpy
import sys
import traceback
from os.path import basename, join

from importlib import import_module
from importlib.abc import SourceLoader, MetaPathFinder
from importlib.util import spec_from_loader

from .lib.out import info, debug

UNSAVED_STRING = "<unsaved .blend>"

DEBUG_IMPORT = False

_loaded_modules = []
_loaded_textblock_paths = []

# ======================================
# =           IMPORTER UTILS           =
# ======================================


def _import_debug(*args):
	"""Utility function to output the partials of the import process"""
	global DEBUG_IMPORT
	if DEBUG_IMPORT:
		print(*args)

def get_textblock_from_module_filepath(filepath):
	if bpy.data.filepath not in filepath: #pragma: no cover
		return None

	name = basename(filepath)

	return get_textblock_from_module_name(name)


def get_textblock_from_module_name(name):
	with_py_extension = name + ".py"

	if with_py_extension in bpy.data.texts:
		return bpy.data.texts[with_py_extension]
	else:
		return bpy.data.texts[name]

	return None

def clean_error_messages(textblock):
	for line in textblock.lines:
		index = line.body.find("# <<<")
		index_e = line.body.find("ERROR", index)

		if index + index_e != -2:
			line.body = line.body[:index]

def clean_sys_modules():
	"""remove the previously imported modules from sys.modules,
	This way the get re-loaded at each execution"""
	global _loaded_modules

	debug("Cleaning previously loaded modules:", _loaded_modules)

	for mod in _loaded_modules:
		try:
			textblock = get_textblock_from_module_name(mod)
			clean_error_messages(textblock)
		except KeyError:
			debug("Textblock for module `{}` not found".format(mod))

		if mod in sys.modules:
			sys.modules.pop(mod)

	_loaded_modules.clear()
	_loaded_textblock_paths.clear()



def add_meta_finder(sznn):
	"""Add the BlendFile_Module_Finder to sys.meta_path if there's no one present"""
	clean_sys_modules()

	for finder in sys.meta_path:
		if type(finder) == BlendFile_Module_Finder:
			return

	finder = BlendFile_Module_Finder(sznn)
	sys.meta_path.append(finder)
	return finder


def get_sznn_globals(sznn, textblock = None ):
	"""Get the globals dict to be injected on the new module"""
	g = {}

	#basics
	if textblock:
		g["__name__"] = "__main__"

		if bpy.data.filepath: #pragma: no cover
			basedir = bpy.data.filepath
		else:
			basedir = UNSAVED_STRING

		g["__file__"] = join(basedir, textblock.name)
		g["__package__"] =  None
		g["__loader__"] =  None
		g["__spec__"] =  None

	#overwrite builtin `print()` to use the blender console
	g["__builtins__"] = globals()["__builtins__"].copy()
	g["__builtins__"]["print"] = info
	
	#Convenience imports
	g["bpy"] = bpy
	g["math"] = import_module("math")
	g["mathutils"] = import_module("mathutils")

	expose_mathutils = ["Color", "Euler", "Vector", "Quaternion", "Matrix"] 
	for m in expose_mathutils:
		g[m] = getattr(g["mathutils"], m)

	#Expose Suzanne_Codes module's global members
	g["sznn"]  = sznn
	for k in sznn.__all__:
		g[k] = getattr( sznn, k )

	return g


def handle_exception(name, textblock):
	"""Handle the exception: Print a simplified version of the error on the 
	Blender console, and mark the corresponding lines in the code"""
	debug("HANDLE EXCEPTION FROM {} | {}".format(name, textblock))
	# Get stack info
	exc_type, exc_obj, tb = sys.exc_info()
	stack_summary = traceback.extract_tb(tb)
	exc_name = exc_type.__name__

	# Handle syntax errors raised by the exec statement
	if exc_type is SyntaxError:
		if exc_obj.filename == name:
			if (exc_obj.lineno - 1) < len(textblock.lines):
				line = textblock.lines[exc_obj.lineno  - 1].body 
				info("Syntax Error on `{}`, line: [{}] pos: {}".format(textblock.name, exc_obj.lineno, exc_obj.offset))
				info(exc_obj)
				info("-" * len(line))
				info(line)
				info("{}^".format(" "*(exc_obj.offset-1)))
				textblock.lines[exc_obj.lineno - 1].body += "# <<< SYNTAX ERROR @ position:{}".format(exc_obj.offset)
				return
			else: #pragma: no branch
				info("Error found outside your code, probably a bug on the library. Please report the error you will find in the system console")
				raise exc_obj    
		else:
			raise exc_obj        

	# Extract errors on user code
	errors_on_user_code = []

	for i,f in enumerate(stack_summary):
		debug("EXECUTE STACK #{}, file: {}, line: {}".format(i, f.filename, f.lineno))
		if f.filename == name:
			if (f.lineno - 1) < len(textblock.lines):
				errors_on_user_code .append((f.lineno, textblock))
			else:
				debug("Cannot mark code at `__main__`, line {}, code length: {}".format(f.lineno-1, len(textblock.lines)))

		elif f.filename in _loaded_textblock_paths:
			err_textblock = get_textblock_from_module_filepath(f.filename)

			if (f.lineno - 1) < len(err_textblock.lines):
				errors_on_user_code .append((f.lineno, err_textblock))
				debug("ERROR ON INTERNAL MODULE:", (f.lineno, err_textblock))
			else:
				debug("Cannot mark code at `{}` line {}, code length: {}".format(err_textblock.name, f.lineno-1, len(err_textblock.lines)))


	#begin from the most recent call
	errors_on_user_code.reverse()
	
	for i, error in enumerate(errors_on_user_code):
		err_line, err_textblock = error
		# Print only the most recent call
		if i == 0:
			line = err_textblock.lines[err_line - 1].body.strip()
			script_name = err_textblock.name

			info("Error on `{}`, line: [{}] {}".format(script_name, err_line, line))
			info("[{}] {}".format(exc_name, exc_obj))

		# Mark the code
		number_calls = ""
		if len(errors_on_user_code) > 1:
			number_calls = "({}) ".format(i)

		err_textblock.lines[err_line - 1].body += "# <<< ERROR {}{}".format(number_calls, exc_name)


	# If the error was originated outside user code raise the exception
	if not errors_on_user_code: #pragma: no branch
		info("Error found outside your code, probably a bug on the SuzanneCodes library. Please report the error printed in the system console")
		raise exc_obj

# ======  End of IMPORTER UTILS  =======


# =========================================
# =           FINDER AND LOADER           =
# =========================================

class BlendFile_Module_Loader(SourceLoader):
	""" Class to load modules from bpy.data.texts datablocks"""
	def __init__(self, suzanne_lib):
		self.sznn = suzanne_lib
		super().__init__()

	def exec_module(self, module):
		"""Inject the sznn exposed properties and methods"""
		g = get_sznn_globals(self.sznn)
		module.__dict__.update(g)		

		try:
			super().exec_module(module)
			
		except Exception as e:
			textblock = get_textblock_from_module_name(module.__name__)
			handle_exception(module.__file__, textblock)


	def get_data(self, path):
		"""An abstract method to return the bytes for the data located at path. 
		Loaders that have a file-like storage back-end that allows storing arbitrary data 
		can implement this abstract method to give direct access to the data stored. 
		OSError is to be raised if the path cannot be found. 
		The path is expected to be constructed using a module’s __file__ attribute 
		or an item from a package’s __path__."""
		_import_debug("on GET_DATA, path: {}".format(path))

		textblock_name = basename(path)

		textblock = get_textblock_from_module_name(textblock_name)

		code = textblock.as_string()
		_import_debug("on GET_DATA, textblock name: {}, \n--BODY--\n{}\n--END BODY--".format(textblock, code))
		
		return bytes(code, 'utf-8')
		
		
	def get_filename(self, fullname):
		"""An abstract method that is to return the value of __file__ 
		for the specified module. 
		If no path is available, ImportError is raised.
		If source code is available, then the method should return 
		the path to the source file, regardless of whether 
		a bytecode was used to load the module."""
		_import_debug("on GET_FILENAME, fullname: {}".format(fullname))
		_import_debug(bpy.data.texts.keys())

		
		with_py_extension = fullname + ".py"

		if with_py_extension in bpy.data.texts:
			_import_debug("on GET_FILENAME, with extension found", with_py_extension)
			# Check if both versions of the name are present at the same time
			if fullname in bpy.data.texts:
				_import_debug("on GET_FILENAME, also raw name found: ERROR!", fullname)

				raise ImportError("Conflicting modules found in this blend file {} <--> {}.\n \
Cannot use the same name with and without '.py' extension.".format(fullname, with_py_extension))

			fullname = with_py_extension

		if bpy.data.filepath: #pragma: no cover
			basedir = bpy.data.filepath
		else:
			basedir = UNSAVED_STRING

		filename = join(basedir, fullname)
		_loaded_textblock_paths.append(filename)

		return filename
		

class BlendFile_Module_Finder(MetaPathFinder):
	""" Class to find modules whitin bpy.data.texts datablocks"""

	def __init__(self, suzanne_lib):
		self.sznn = suzanne_lib
		super().__init__()
	
	def find_spec(self, fullname, path, target=None):
		"""An abstract method for finding a spec for the specified module. 
		If this is a top-level import, path will be None. 
		Otherwise, this is a search for a subpackage or module and path 
		will be the value of __path__ from the parent package. 
		If a spec cannot be found, None is returned. 
		When passed in, target is a module object that the finder may use 
		to make a more educated guess about what spec to return. 
		importlib.util.spec_from_loader() may be useful for implementing 
		concrete MetaPathFinders."""
		global _loaded_modules

		_import_debug("on FIND_SPEC: fullname:{}, path:{}, target:{}".format(fullname, path, target))

		# Test if the requested module name is present in the blendfile as a textblock
		# The test includes the name with the ".py extension" 	
		with_py_extension = fullname + ".py"
		if fullname in bpy.data.texts or with_py_extension in bpy.data.texts:
			_import_debug("on FIND_SPEC: found module {} in blendfile!".format(fullname))

			_loaded_modules.append(fullname)

			return spec_from_loader(fullname, BlendFile_Module_Loader(self.sznn))
		else:
			_import_debug("on FIND_SPEC: module {} NOT FOUNT in blendfile, ERROR!".format(fullname))
			return None
   

# ======  End of FINDER AND LOADER  =======


			
if __name__ == "__main__":
	#Just for quick tests, mostly unused

	sys.meta_path.append(BlendFile_Module_Finder())

	import fake_imported_mod
	fake_imported_mod.__name__

	sys.modules["fake_imported_mod"]

	from importlib import reload
	reload(fake_imported_mod)

	fake_imported_mod.this_func()

