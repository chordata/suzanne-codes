# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy

class SuzanneCodesOBProps(bpy.types.PropertyGroup):
	is_created: bpy.props.BoolProperty(name = "Created by sznn", 
		description = "This object was created with Suzanne Codes", default = False)
	
	is_used: bpy.props.BoolProperty(name = "Used by sznn", 
		description = "This object is used by Suzanne Codes", default = False)


class SuzanneCodesWinManProps(bpy.types.PropertyGroup):
	modal_running: bpy.props.BoolProperty(name = "Sznn modal operator running", default = False)
	
	modal_stop: bpy.props.BoolProperty(name = "Flag for stopping sznn modal operator", default = False)

	remove_prev_obs: bpy.props.BoolProperty(name = "Remove previous sznn objects", 
		description="Remove objects that were previouly created by Suzanne.codes", default = True)



	# Q_temp_received: bpy.props.FloatVectorProperty(name="Temporary received quat", 
	# 	description="The Chordata engine uses this temporary prop to place the received quaternion data before processing it.", 
	# 	default=(1, 0, 0, 0), size=4, subtype='QUATERNION')

	# calibration_state: bpy.props.EnumProperty(name="Calibration state", 
	# 	description="The pose-calibration state of this bones", 
	# 	items=defaults.CALIBRATION_STATES, default=defaults.CALIBRATION_DEF_STATE )

	# mag_covariance: bpy.props.FloatProperty(name = "Bone magnetic covariance", 
	# 	 default = 0)


def register():
	bpy.utils.register_class(SuzanneCodesOBProps)
	bpy.types.Object.sznn = bpy.props.PointerProperty(name="SuzanneCodes object properties",
							description="SuzanneCodes object properties",
							type=SuzanneCodesOBProps)

	bpy.utils.register_class(SuzanneCodesWinManProps)
	bpy.types.WindowManager.sznn = bpy.props.PointerProperty(name="SuzanneCodes winman properties",
							description="SuzanneCodes winman properties", options={'SKIP_SAVE'},
							type=SuzanneCodesWinManProps)


def unregister():
	bpy.utils.unregister_class(SuzanneCodesOBProps)
	bpy.utils.unregister_class(SuzanneCodesWinManProps)
