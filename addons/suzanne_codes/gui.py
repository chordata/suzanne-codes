# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy




class TEXT_PT_Suzanne_Code(bpy.types.Panel):
    bl_space_type = 'TEXT_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Text"
    bl_label = "Suzanne.codes"

    def draw(self, context):
        layout = self.layout
        # layout.operator("suzanne_code.main_op", text="Suzanne.code RUN")
        layout.operator("suzanne_code.run", text="Run", icon="PLAY")

        layout.operator("suzanne_code.stop", text="Stop", icon="SNAP_FACE")

        if "sznn: objects" in bpy.data.collections:
            layout.prop(context.window_manager.sznn, "remove_prev_obs")

        


def register():
    bpy.utils.register_class(TEXT_PT_Suzanne_Code)



def unregister():
    bpy.utils.unregister_class(TEXT_PT_Suzanne_Code)
