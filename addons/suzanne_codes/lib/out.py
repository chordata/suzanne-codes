# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy


def _get_workspace(workspace):
    if workspace == "current":
        return bpy.context.workspace
    else:
        if workspace not in bpy.data.workspaces.keys():
            raise KeyError("No workspace named '{}'".format(workspace))
        
        return bpy.data.workspaces[workspace]


# def nodeeditor_spaces_get( workspace = "Mocap" ):
#     workspace = _get_workspace(workspace)

#     nodeed_spaces = []
    
#     for screen in workspace.screens:
#         for area in screen.areas:
#             if area.type == 'NODE_EDITOR':
#                 if area.ui_type == 'ChordataTreeType': #pragma: no branch
#                     for space in area.spaces:
#                         if space.type == 'NODE_EDITOR':
#                             nodeed_spaces.append(space)
                            
#     return nodeed_spaces 


# https://blender.stackexchange.com/questions/78330/how-can-i-display-what-happens-in-the-system-console-inside-blenders-console
def console_get( workspace = "current" ):
    workspace = _get_workspace(workspace)

    for screen in workspace.screens:
        for area in screen.areas:
            if area.type == 'CONSOLE':
                for space in area.spaces: #pragma: no branch
                    if space.type == 'CONSOLE':  #pragma: no branch
                        return area, space
    return None, None


def console_write(text, workspace = "current"):
    area, space = console_get( workspace )
    if space is None:
        return False

    context = bpy.context.copy()
    context.update(dict(
        space=space,
        area=area,
    ))
    for line in text.split("\n"):
        bpy.ops.console.scrollback_append(context, text=line, type='OUTPUT')

    return True

def console_clear(workspace = "current"):
    area, space = console_get( workspace )
    if space is None:
        return False

    context = bpy.context.copy()
    context.update(dict(
        space=space,
        area=area,
    ))
    
    bpy.ops.console.clear(context)


def debug(*args):
    print("[DEBUG]:", *args)

def info(*args):
    print("[INFO]:", *args)
    args = [str(a) for a in args]
    console_write(" ".join(args))

def warning(*args):
    print("[WARNING]:", *args)
    args = [str(a) for a in args]
    console_write("[WARNING]:" + " ".join(args))