# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 



if "bpy" in locals():
	import importlib
	importlib.reload(out)
	importlib.reload(getters)
	importlib.reload(state)
	importlib.reload(utils)

else:
	from . import out
	from . import getters
	from . import state
	from . import utils


import bpy
import sys
from datetime import datetime
from .. import bl_info

# ======================================
# =           CORE FUNCTIONS           =
# ======================================

version = "v{}.{}.{}({})".format(*bl_info["version"], state.GIT_HASH)

def time_str():
	return datetime.now().isoformat(sep=' ', timespec="seconds")

def __suzanne_code_init__():
	out.console_clear()
	out.console_write(" == Suzanne Codes INIT {} @ {} == \n".format(version, time_str()))
	getters.remove_created_objects()
	state._reset_sznn_state()
	state._set_sznn_state("stage", state.SuzanneCodes_STAGES.SETUP)
	state._modal = None
	state._timekeeper = None

def __suzanne_code_run__(module_vars):
	state._timekeeper = utils.Timekeeper()
	state._set_sznn_state("stage", state.SuzanneCodes_STAGES.RUN)

	if "modal" in module_vars.keys():
		state._modal = module_vars["modal"]
		bpy.ops.suzanne_code.modal_op('INVOKE_DEFAULT')


def __suzanne_code_close__():
	state._timekeeper = None
	out.console_write("\n == Suzanne Codes END @ {} == \n".format(time_str()))


__all__ = ["__suzanne_code_run__", "__suzanne_code_init__", "__suzanne_code_close__"]

# ======  End of CORE FUNCTIONS  =======


# ===========================================
# =           PUBLIC from modules           =
# ===========================================

def add_to_API(module):
	global __all__
	__all__ += module.__export__

	d = {name: getattr(module, name) for name in module.__export__}
	globals().update(d)

add_to_API(getters)
add_to_API(state)
add_to_API(utils)




# ======  End of PUBLIC from modules  =======