# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import mathutils
from enum import Enum, auto
from collections.abc import Iterable
from ..utils import _deg_to_rad

# ====================================
# =           GET VARIADIC TYPE           =
# ====================================

class Variadic_TRANSFORM_type(Enum):
	XYZ = 0
	X = 1
	Y = 2
	Z = 3
	XY = 4
	XZ = 5
	YZ = 6
	SINGLE = 7
	# VECTOR = 8
	# TUPLE = 9
	# EULER = 10
	# QUATERNION = 11
	# MATRIX = 12

def _get_variadic_type(valid_keys, single_key, args, kwargs):
	"""Shared implementation to get location, rotation and scale variadic type from arguments"""

	# nonlocal single_key, valid_keys

	# -----------  ARGS  -----------
	
	if args and not kwargs:
		if len(args) == 3:
			return Variadic_TRANSFORM_type.XYZ 
		elif len(args) == 1:
			return Variadic_TRANSFORM_type.SINGLE
		
		raise TypeError("Expected 3 (XYZ) or 1 (SINGLE) arguments, {} given instead".format(len(args)))
	
	elif args and kwargs:
		raise TypeError("Cannot mix positional ({}) and keyword arguments: {}".format(len(args), kwargs))
	elif not args and not kwargs:
		raise TypeError("No arguments found, you have to pass at least one argument representing a valid transformation")

	# -----------  KWARGS  -----------
	dims = ""

	for k in kwargs:
		if k not in valid_keys:
			raise TypeError("The keyword argument [{}] is not valid".format(k))
			
		dims += k.upper()

	# Test for only rotation arg
	if dims == single_key:
		return Variadic_TRANSFORM_type.SINGLE
		
	# Test for x,y,z without rotation arg
	dims = ''.join(sorted(dims))
		
	try:
		xyz_type = Variadic_TRANSFORM_type[dims]
		return xyz_type
	except KeyError:
		wrong_kw = [k for k in kwargs if k not in ["x", "y", "z"]]
   
		raise TypeError("The key `{}` cannot be used togheter with `x`, `y` or `z`".format(wrong_kw))

 

valid_rot_keys = ("x", "y", "z", "rotation")
rot_key = "rotation".upper()

def get_variadic_ROT_type(args, kwargs):
	global valid_rot_keys, rot_key
	return _get_variadic_type(valid_rot_keys, rot_key, args, kwargs)


valid_loc_keys = ("x", "y", "z", "location")
loc_key = "location".upper()

def get_variadic_LOC_type(args, kwargs):
	global valid_loc_keys, loc_key
	return _get_variadic_type(valid_loc_keys, loc_key, args, kwargs)


# =======================================
# =           GET FROM SINGLE           =
# =======================================


def get_euler_from_single_ROT(rot, rot_type):
	if isinstance(rot, mathutils.Euler):
		return rot

	elif isinstance(rot, (mathutils.Quaternion, mathutils.Matrix)):
		return rot.to_euler()

	if isinstance(rot, Iterable):
		if len(rot) == 3:
			rot = [_deg_to_rad(val) for val in rot]
			return mathutils.Euler(rot)

		elif len(rot) == 4:
			return mathutils.Quaternion(rot).to_euler()

	raise TypeError("The value `{}` of type `{}` doesn't represent a valid rotation".format(rot, type(rot)))


def get_vec_from_single_LOC(loc, loc_type):
	if isinstance(loc, mathutils.Vector):
		return loc

	if isinstance(loc, mathutils.Matrix):
		return loc.to_translation()

	if isinstance(loc, Iterable):
		if len(loc) == 3:
			return mathutils.Vector(loc)

	raise TypeError("The value `{}` of type `{}` doesn't represent a valid location".format(loc, type(loc)))


def get_vec_from_single_SCALE(scale, scale_type):
	if isinstance(scale, mathutils.Vector):
		return scale

	if isinstance(scale, mathutils.Matrix):
		return scale.to_scale()

	if isinstance(scale, Iterable):
		if len(scale) == 3:
			return mathutils.Vector(scale)

	raise TypeError("The value `{}` of type `{}` doesn't represent a valid scale".format(scale, type(scale)))


# ======  End of GET FROM SINGLE  =======

