# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 


if "bpy" in locals():
	import importlib
	importlib.reload(t)
	importlib.reload(out)
	importlib.reload(getters)


else:
	from ..utils import _deg_to_rad
	from ..state import _get_sznn_state
	from .. import out
	from .. import getters
	from . import arg_types as t

import bpy
import mathutils



class SuzanneCode_OB:
	"""This class wraps native Blender objects offering simplified methods for the common operations.
	It should be as transparent to the user as possible, exposing the common native properties directly.
	
	## Current exposed properties and setters:
	- Name
	- Location
	- Rotation
	- Scale

	## Todo exposed properties and setters:
	- Material (only first slot, and as a wrapper to a node-based principled shader) 
	- Mesh (as a wrapper with easy access to mesh elements)
	"""
	# ================================
	# =           DAFAULTS           =
	# ================================
	
	_look_at_axis_def = ["-Y", "Z"]
	_go_towards_speed_def = 0.2
	
	# ======  End of DAFAULTS  =======

	def __init__(self, ob:bpy.types.Object):
		self._ob = ob
		self._ob_name = ob.name
		self._parent = None
		self._evaluated = None
		self._primitive = None

		if ob.parent:
			self._parent = getters.get_object(ob.parent)			

		ob.sznn.is_used = True

		if self._ob.rotation_mode != 'XYZ':
			out.warning("The rotation mode ({}) of the object ({}) is not compatible with the SuzanneCodes library. You might get unexpected rotation results.".format(self._ob.rotation_mode, self._ob.name))

	def remove(self):
		if self.data and self.data.users == 1:
			if isinstance(self.data, bpy.types.Mesh):
				bpy.data.meshes.remove(self.data)			

		if self._ob_name in bpy.data.objects:
			bpy.data.objects.remove(self.raw)

		self._ob = None

	def _clean_ref(self):
		self._ob = None
		self._evaluated = None

	def __repr__(self):
		return "{}.ob.SuzanneCode_OB('{}')".format(__package__, self.name)

	def __str__(self):
		if not hasattr(self, "_str"):
			self._str = "sznn: {}".format(self.raw)
		
		return self._str

	def __eq__(self, other):
		if isinstance(other, bpy.types.Object):
			return self.raw == other
		elif isinstance(other, self.__class__):
			return self.raw == other._ob

		return NotImplemented

	def __hash__(self):
		return self.raw.__hash__()
	
	@property
	def raw(self):
		if not self._ob:
			try:
				self._ob = bpy.context.scene.objects[self._ob_name]
			except KeyError:
				raise NameError("No object `{}` found in the current scene, perhaps it was removed or renamed".format(self._ob_name))

		return self._ob

	@property
	def type(self):
		return self.raw.type

	@property
	def data(self):
		return self.raw.data

	@property
	def mesh(self):
		raise NotImplementedError("In order to access the raw mesh datablock use the `data` property")

	@property
	def material(self):
		raise NotImplementedError("In order to access the materials use the `raw.material_slots` collection")

	def evaluate(self, context = bpy.context):
		dps_ctx = context.evaluated_depsgraph_get()
		dps_ctx.update() 
		self._evaluated = self.raw.evaluated_get(dps_ctx)

	@property
	def matrix(self, *,update=False):
		if self._evaluated:
			return self._evaluated.matrix_world

		return self.raw.matrix_world

	@property
	def is_created(self):
		return self.raw.sznn.is_created
	

	# -----------  Name  -----------
	
	@property
	def name(self):
		return self.raw.name


	# -----------  Parent  -----------

	@property
	def parent(self):
		if not self._parent and not self.raw.parent:
			return self._parent
		if not self._parent and self.raw.parent:
			self._parent = getters.get_object(self.raw.parent)
		elif self.raw.parent != self._parent.raw:
			self._parent = getters.get_object(self.raw.parent)

		return self._parent


	def set_parent(self, parent):
		if isinstance( parent, self.__class__):
			self._parent = parent
			self.raw.parent = parent.raw

		elif isinstance( parent, bpy.types.Object):
			self._parent =  getters.get_object(parent)
			self.raw.parent = parent

		else:
			raise TypeError("Cannot set the parent of `{}`, to an object of type {}".format(self.name, type(parent)))

		return self
	
	
	# -----------  Rotation  -----------
	

	@property
	def rotation(self):
		return self.raw.rotation_euler

	@property
	def rotation_euler(self):
		return self.raw.rotation_euler

	@property
	def rotation_quaternion(self):
		self.raw.rotation_quaternion = self.raw.rotation_euler.to_quaternion()
		return self.raw.rotation_quaternion

	def _set_rotationXYZ(self, x = None, y = None, z = None):
		if x is not None:
			self.raw.rotation_euler.x = _deg_to_rad(x)

		if y is not None:
			self.raw.rotation_euler.y = _deg_to_rad(y)

		if z is not None:
			self.raw.rotation_euler.z = _deg_to_rad(z)


	def set_rotation(self, *args, **kwargs):
		"""Set the rotation of the object"""
		rot_type = t.get_variadic_ROT_type(args, kwargs) 

		rot = None

		if args:
			if rot_type == t.Variadic_TRANSFORM_type.XYZ:
				self._set_rotationXYZ(*args)
				return self
			else:
				rot = args[0]

		else: #Use kwargs
			if rot_type.value < t.Variadic_TRANSFORM_type.SINGLE.value:
				self._set_rotationXYZ(**kwargs)
				return self
			else:
				# test for "rotation" in kwargs done in t.get_variadic_ROT_type()
				rot = kwargs["rotation"]

		e = t.get_euler_from_single_ROT(rot, rot_type)
		self.raw.rotation_euler = e
		return self


	def set_rotationX(self, val):
		self.raw.rotation_euler.x = _deg_to_rad(val)
		return self

	def set_rotationY(self, val):
		self.raw.rotation_euler.y = _deg_to_rad(val)
		return self

	def set_rotationZ(self, val):
		self.raw.rotation_euler.z = _deg_to_rad(val)
		return self


	def _rotateXYZ(self, x = 0, y = 0, z = 0):
		vals = (_deg_to_rad(x), _deg_to_rad(y), _deg_to_rad(z))
		to_rotate = mathutils.Euler(vals)
		self.raw.rotation_euler.rotate(to_rotate)


	def rotate(self, *args, **kwargs):
		"""Rotate the object"""
		rot_type = t.get_variadic_ROT_type(args, kwargs) 

		rot = None

		if args:
			if rot_type == t.Variadic_TRANSFORM_type.XYZ:
				self._rotateXYZ(*args)
				return self
			else:
				rot = args[0]

		else: #Use kwargs
			if rot_type.value < t.Variadic_TRANSFORM_type.SINGLE.value:
				self._rotateXYZ(**kwargs)
				return self
			else:
				# test for "rotation" in kwargs done in t.get_variadic_ROT_type()
				rot = kwargs["rotation"]

		e = t.get_euler_from_single_ROT(rot, rot_type)
		self.raw.rotation_euler.rotate(e)

		return self


	def rotateX(self, val):
		self.raw.rotation_euler.x += _deg_to_rad(val)
		return self

	def rotateY(self, val):
		self.raw.rotation_euler.y += _deg_to_rad(val)
		return self

	def rotateZ(self, val):
		self.raw.rotation_euler.z += _deg_to_rad(val)
		return self

	# -----------  GENERIC VARIADIC VECTOR HANDLER -----------
	
	def _generic_var_vec_handler(self, _setXYZ, _setSingle, args, kwargs, _extra = None):
		"""This private handler contains the basic logic used on all 
		the transform handlers acepting a vector as input in variadic form"""
		loc_type = t.get_variadic_LOC_type(args, kwargs)

		loc = None

		if args:
			if loc_type == t.Variadic_TRANSFORM_type.XYZ:
				_setXYZ(*args, _extra)
				return self
			else:
				loc = args[0]

		else: #Use kwargs
			if loc_type.value < t.Variadic_TRANSFORM_type.SINGLE.value:
				_setXYZ(**kwargs, extra = _extra)
				return self
			else:
				# test for "location" in kwargs done in t.get_variadic_LOC_type()
				loc = kwargs["location"]

		v = t.get_vec_from_single_LOC(loc, loc_type)
		_setSingle(v, _extra)

	# -----------  LOOK_AT -----------
	
	def look_at(self, *args, **kwargs):
		"""Rotate the object to point to the given location. 
		The `direction` arg controls the local axis that will point to the location.
		The `up` arg controls the local axis that will point up."""
		axis = self._look_at_axis_def

		if "direction" in kwargs:
			axis[0] = kwargs.pop("direction")

		if "up" in kwargs:
			axis[1] = kwargs.pop("up")

		elif len(args) != 1 and len(args) != 3:
			raise TypeError("The arguments are invalid. The `look_at` function accepts 3 (x,y,z) or 1 (3D point) positional arguments. The `direction` and `up` arguments should be passed as kewyword arguments")

		self._generic_var_vec_handler(self._look_atXYZ, self._look_atVector, args, kwargs, axis )
		return self
	
	def _look_atVector(self, target, axis):
		delta_pos = (target - self.raw.location)
		q = delta_pos.to_track_quat(*axis)
		self.raw.rotation_euler = q.to_euler()

	def _look_atXYZ(self, x = None, y = None, z = None, extra = None):
		target = self.raw.location.copy()

		if x is not None:
			target.x = x

		if y is not None:
			target.y = y

		if z is not None:
			target.z = z

		self._look_atVector(target, extra)


	# -----------  Location  -----------
	
	@property
	def location(self):
		return self.raw.location

	@location.setter
	def location(self, new_loc):
		self.raw.location = new_loc

	def set_location(self, *args, **kwargs):
		"""Set the location of the object."""
		self._generic_var_vec_handler(self._set_locationXYZ, self._set_locationVector, args, kwargs )
		return self


	def _set_locationXYZ(self, x = None, y = None, z = None, extra = NotImplemented):
		if x is not None:
			self.raw.location.x = x

		if y is not None:
			self.raw.location.y = y

		if z is not None:
			self.raw.location.z = z

	def _set_locationVector(self, v, extra = NotImplemented):
		self.raw.location = v

	def set_locationX(self, val):
		self.raw.location.x = val
		return self

	def set_locationY(self, val):
		self.raw.location.y = val
		return self


	def set_locationZ(self, val):
		self.raw.location.z = val
		return self

	# -----------  MOVE  -----------
	
	def move(self, *args, **kwargs):
		"""Move the object"""
		self._generic_var_vec_handler(self._moveXYZ, self._moveVector, args, kwargs )
		return self

	def _moveXYZ(self, x = None, y = None, z = None, extra = NotImplemented):
		if x is not None:
			self.raw.location.x += x

		if y is not None:
			self.raw.location.y += y

		if z is not None:
			self.raw.location.z += z

	def _moveVector(self, v, extra = NotImplemented):
		self.raw.location += v

	def moveX(self, val):
		self.raw.location.x += val
		return self

	def moveY(self, val):
		self.raw.location.y += val
		return self

	def moveZ(self, val):
		self.raw.location.z += val
		return self

	# -----------  GO TOWARDS  -----------
	
	def go_towards(self, *args, **kwargs):
		"""Go towards a given point at a certain speed"""
		speed = self._go_towards_speed_def 
		if "speed" in kwargs:
			speed = kwargs.pop("speed")

		elif len(args) == 2 or len(args) == 4:
			speed = args[-1]
			args = args[:-1]

		self._generic_var_vec_handler(self._go_towardsXYZ, self._go_towardsVector, args, kwargs, speed)
		return self

	def _go_towardsVector(self, target, speed):
		delta_pos = (target - self.raw.location)
		vel  = delta_pos.normalized()
		vel *= speed
		
		if vel > delta_pos:
			vel = delta_pos

		self.raw.location += vel

	def _go_towardsXYZ(self, x = None, y = None, z = None, extra = None):
		go_tow = self.raw.location.copy()

		if x is not None:
			go_tow.x = x

		if y is not None:
			go_tow.y = y

		if z is not None:
			go_tow.z = z

		self._go_towardsVector(go_tow, extra)

	# -----------  SET SCALE  -----------
	
	@property
	def scale(self):
		return self.raw.scale
	
	def set_scale(self, *args, **kwargs):
		"""Set the scale of the object."""
		#re-use the get_variadic_LOC_type()
		if "scale" in kwargs:
			kwargs["location"] = kwargs.pop("scale") 

		scale_type = t.get_variadic_LOC_type(args, kwargs)

		scale = None

		if args:
			if scale_type == t.Variadic_TRANSFORM_type.XYZ:
				self._set_scaleXYZ(*args)
				return self
			else:
				scale = args[0]

		else: #Use kwargs
			if scale_type.value < t.Variadic_TRANSFORM_type.SINGLE.value:
				self._set_scaleXYZ(**kwargs)
				return self
			else:
				# test for "location" in kwargs done in t.get_variadic_LOC_type()
				scale = kwargs["location"]

		v = t.get_vec_from_single_SCALE(scale, scale_type)
		self._set_scaleVector(v)
		return self


	def _set_scaleXYZ(self, x = None, y = None, z = None, extra = NotImplemented):
		if x is not None:
			self.raw.scale.x = x

		if y is not None:
			self.raw.scale.y = y

		if z is not None:
			self.raw.scale.z = z

	def _set_scaleVector(self, v, extra = NotImplemented):
		self.raw.scale = v

	def set_scaleX(self, val):
		self.raw.scale.x = val
		return self

	def set_scaleY(self, val):
		self.raw.scale.y = val
		return self

	def set_scaleZ(self, val):
		self.raw.scale.z = val
		return self

# -----------  CHANGE SCALE  -----------
	
	def change_scale(self, *args, **kwargs):
		"""Change the scale of the object."""
		#re-use the get_variadic_LOC_type()
		if "scale" in kwargs:
			kwargs["location"] = kwargs.pop("scale") 
		scale_type = t.get_variadic_LOC_type(args, kwargs)

		scale = None

		if args:
			if scale_type == t.Variadic_TRANSFORM_type.XYZ:
				self._change_scaleXYZ(*args)
				return self
			else:
				scale = args[0]

		else: #Use kwargs
			if scale_type.value < t.Variadic_TRANSFORM_type.SINGLE.value:
				self._change_scaleXYZ(**kwargs)
				return self
			else:
				# test for "location" in kwargs done in t.get_variadic_LOC_type()
				scale = kwargs["location"]

		v = t.get_vec_from_single_SCALE(scale, scale_type)
		self._change_scaleVector(v)
		return self
		return self

	def _change_scaleXYZ(self, x = None, y = None, z = None):
		if x is not None:
			self.raw.scale.x += x

		if y is not None:
			self.raw.scale.y += y

		if z is not None:
			self.raw.scale.z += z

	def _change_scaleVector(self, v):
		self.raw.scale += v

	def change_scaleX(self, val):
		self.raw.scale.x += val
		return self

	def change_scaleY(self, val):
		self.raw.scale.y += val
		return self

	def change_scaleZ(self, val):
		self.raw.scale.z += val
		return self

	# =============================
	# =           UTILS           =
	# =============================
	
	def _apply_transforms_and_parent(self, location=None, rotation=None, scale=None, parent=None):
		if location:
			self.set_location(location)

		if rotation:
			self.set_rotation(rotation)

		if scale:
			self.set_scale(scale)

		if parent:
			self.set_parent(parent)
	
	# ======  End of UTILS  =======
	

