# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 


if "bpy" in locals():
	import importlib
	importlib.reload(wrap)
	importlib.reload(state)

else:
	from . import wrap
	from . import state


import bpy
import bmesh
import functools 

def clean_ob_refs():
	for cleaner in state._refs_to_clean:
		cleaner()	


def get_object(name_or_ob):
	cache = state._get_sznn_state("ob_cache")

	if isinstance(name_or_ob, str):
		if not name_or_ob in bpy.context.scene.objects:
			raise NameError("No object `{}` found in the current scene".format(name_or_ob))

		if name_or_ob in cache:
			return cache[name_or_ob]

		ob = wrap.ob.SuzanneCode_OB(bpy.context.scene.objects[name_or_ob])
		cache[name_or_ob] = ob

	elif isinstance(name_or_ob, bpy.types.Object):
		if not name_or_ob.name in bpy.context.scene.objects:
			raise NameError("The object `{}` doesn't belong to the current scene".format(name_or_ob.name))
		
		if name_or_ob.name in cache:
			return cache[name_or_ob.name]

		ob =  wrap.ob.SuzanneCode_OB(name_or_ob)
		cache[name_or_ob.name] = ob

	elif isinstance(name_or_ob, wrap.ob.SuzanneCode_OB):
		
		if name_or_ob.name in cache:
			return cache[name_or_ob.name]

		cache[name_or_ob.name] = name_or_ob
		ob = name_or_ob 

	else: 
		raise TypeError("The argument {}, of type {} is inapropiate to get an object from the scene. It should be a string or another object".format(name_or_ob, type(name_or_ob)))

	state._refs_to_clean.append(ob._clean_ref)
	return ob


calc_uvs = False

_mesh_primitives = { 
	"PLANE": lambda bm: bmesh.ops.create_grid(bm, x_segments=1, y_segments=1, size=1, calc_uvs=calc_uvs),
	"CUBE": lambda bm: bmesh.ops.create_cube(bm, size=2, calc_uvs=calc_uvs),
	"CIRCLE": lambda bm: bmesh.ops.create_circle(bm, segments=32, radius=1, calc_uvs=calc_uvs),
	"UVSPHERE": lambda bm: bmesh.ops.create_uvsphere(bm, u_segments=32, v_segments=16, diameter=1.0, calc_uvs=calc_uvs),
	"ICOSPHERE": lambda bm: bmesh.ops.create_icosphere(bm, subdivisions=2, diameter=1, calc_uvs=calc_uvs),
	"CYLINDER": lambda bm: bmesh.ops.create_cone(bm, cap_ends=True, cap_tris=True, segments=32, diameter1=1, diameter2=1, depth=2, calc_uvs=calc_uvs),
	"CONE": lambda bm: bmesh.ops.create_cone(bm, cap_ends=True, cap_tris=True, segments=32, diameter1=1, diameter2=0, depth=2, calc_uvs=calc_uvs),
	# "TORUS": lambda: NotImplemented, #<-- could not find a suitable bmesh.ops to create a torus 
	"GRID": lambda bm: bmesh.ops.create_grid(bm, x_segments=10, y_segments=10, size=1, calc_uvs=calc_uvs),
	"MONKEY": lambda bm: bmesh.ops.create_monkey(bm),
}


def create_object(name:str = "sznn: Object.000" , mesh="MONKEY", *, 
					location=None, rotation=None, scale=None, parent=None ):
	global _mesh_primitives

	if not mesh or mesh == "EMPTY":
		m = None
		mesh = 'EMPTY'

	elif isinstance(mesh, str):
		mesh = mesh.upper()
		if mesh not in _mesh_primitives:
			raise KeyError("The argument `{}` in not a valid mesh primitive. Should be one of: {} ".format(mesh, " | ".join(list(_mesh_primitives.keys()) + ["EMPTY"])))

		bm = bmesh.new()
		m = bpy.data.meshes.new(name)
		
		_mesh_primitives[mesh](bm)
		bm.to_mesh(m)
	
		bm.free()

	elif isinstance(mesh, bpy.types.Mesh):
		m = mesh
		mesh = 'CUSTOM'	

	else:
		raise TypeError("The `mesh` argument should be a string, bpy_types.Mesh or None, `{}` found".format(type(mesh)))
		

	ob = bpy.data.objects.new( name , m )

	if "sznn: objects" not in bpy.data.collections :
		sznn_coll= bpy.data.collections.new("sznn: objects")
		bpy.context.scene.collection.children.link(sznn_coll)
	else:
		sznn_coll = bpy.data.collections["sznn: objects"]

	sznn_coll.objects.link(ob)

	ob.sznn.is_created = True

	obw = wrap.ob.SuzanneCode_OB(ob)
	state._get_sznn_state("ob_cache")[ob.name] = obw

	obw._primitive = mesh
	obw._apply_transforms_and_parent(location, rotation, scale, parent)

	state._refs_to_clean.append(obw._clean_ref)
	return obw


def create_empty(name = "sznn: Empty.000",  **kwargs):
	return create_object(name, None, **kwargs)


def remove_created_objects():
	if not bpy.context.window_manager.sznn.remove_prev_obs:
		return

	cache = state._get_sznn_state("ob_cache")
	for ob in bpy.data.objects:
		if ob.sznn.is_created:
			if ob.name in cache:
				obw = cache[ob.name]
			else:
				obw = get_object(ob.name)

			obw.remove()

	state._refs_to_clean.clear()


def S(name = "sznn: Object.000", mesh="MONKEY", **kwargs):

	if name and name in bpy.context.scene.objects:
		ob = get_object(name)
		if ob.is_created and ob._primitive != mesh:
			return create_object(name, mesh, **kwargs)

		ob._apply_transforms_and_parent(**kwargs)
		return ob

	return create_object(name, mesh, **kwargs)




__export__ = ["S", "get_object", "create_object", "create_empty", "remove_created_objects"] 