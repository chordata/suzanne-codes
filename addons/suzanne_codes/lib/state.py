# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from enum import Enum, auto

_modal = None
_timekeeper = None
_refs_to_clean = []

GIT_HASH = 'dev' # <-- this value is set by the CI pipeline when creating .zip for release 

class SuzanneCodes_STAGES(Enum):
	IDLE = 0
	SETUP = 1
	RUN = 2
	MODAL = 3

__sznn_default_state__ = {
	# "error_encountered" : False,
	"stage": SuzanneCodes_STAGES.IDLE,
	"ob_cache" : {}
}

__sznn_global_state__ =  __sznn_default_state__.copy()

def _reset_sznn_state():
	global __sznn_global_state__, __sznn_default_state__ , _modal
	__sznn_global_state__ =  __sznn_default_state__.copy()
	if bpy.context.window_manager.sznn.remove_prev_obs:
		__sznn_global_state__["ob_cache"].clear()	
		_refs_to_clean.clear()

def _get_sznn_state(k = None):
	if not k:
		return __sznn_global_state__
	else: 
		return __sznn_global_state__[k]


def _set_sznn_state(k, val):
	__sznn_global_state__[k] = val

__export__ = ["_set_sznn_state", "_get_sznn_state", "_reset_sznn_state"] 


def millis():
	global _timekeeper
	return	int(_timekeeper.ellapsed_time() * 1000)

def seconds():
	global _timekeeper
	return	_timekeeper.ellapsed_time()

def frame_rate():
	global _timekeeper
	return	round(_timekeeper.get_rate())

def frame_count():
	global _timekeeper
	return	_timekeeper.lap_n

def set_fps(val):
	if _get_sznn_state("stage") == SuzanneCodes_STAGES.SETUP:
		bpy.context.scene.render.fps = int(val)
		return

	raise RuntimeError("The FPS of the scene can only be set on the body of the script (SETUP stage). \
This function was called on the ({} stage)". format(_get_sznn_state("stage")))

def get_fps():
	return bpy.context.scene.render.fps

__export__ += ["millis", "seconds", "frame_rate", "frame_count", "set_fps", "get_fps"]

