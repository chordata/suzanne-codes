# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import math
import time
from collections.abc import Iterable
from collections import deque
from functools import reduce


# ======================================
# =           CONVERT ANGLES           =
# ======================================

_TAU = math.pi * 2
_RAD_TO_DEG = 360 / _TAU
_DEG_TO_RAD = _TAU / 360

def _rad_to_deg(x):
	return x * _RAD_TO_DEG

def _deg_to_rad(x):
	return x * _DEG_TO_RAD

def rad_to_deg(x):
	if isinstance(x, Iterable):
		ret = []
		for num in x:
			if isinstance(num,(int, float)):
				ret.append(_rad_to_deg(num))
			else:
				raise TypeError("Non numeric value found on conversion from radians to degrees: {}".format(num))
		return ret
	else:
		if isinstance(x,(int, float)):
			return _rad_to_deg(x)
		else:
			raise TypeError("Value of type {} not valid for conversion from radians to degrees".format(type(num)))

def deg_to_rad(x):
	if isinstance(x, Iterable):
		ret = []
		for num in x:
			if isinstance(num,(int, float)):
				ret.append(_deg_to_rad(num))
			else:
				raise TypeError("Non numeric value found on conversion from radians to degrees: {}".format(num))
		return ret
	else:
		if isinstance(x,(int, float)):
			return _deg_to_rad(x)
		else:
			raise TypeError("Value of type {} not valid for conversion from radians to degrees".format(type(num)))

__export__ = ["deg_to_rad", "rad_to_deg"]

# ======  End of CONVERT ANGLES  =======

# ==================================
# =           TIMEKEEPER           =
# ==================================


class Timekeeper:
	def __init__(self, max_queue_len = 20):
		self.max_queue_len = max_queue_len
		self.restart()

	def set_checkpoint(self):
		self.checkpoint = time.time()

	def get_checkpoint_delta(self):
		return time.time() - self.checkpoint

	def lap(self):
		current = time.time()
		self.hist.append(current - self.last)
		self.last = current
		self.lap_n +=1

	def avg_lap(self):
		total = reduce(lambda x, y: x+y, self.hist)
		return total / len(self.hist)

	def get_rate_avg(self):
		return 1/self.avg_lap()

	def get_last_lap(self):
		return (self.hist[0] + self.hist[1]) / 2

	def get_rate(self):
		return self.get_rate_avg()

	def restart(self):
		self.start = time.time()
		self.checkpoint = self.start 
		self.hist = deque(maxlen = self.max_queue_len)  
		self.hist.append(1/25)
		self.last = self.start
		self.lap_n = 0

	def set_queue_len(self, new_length):
		self.max_queue_len = new_length

	def ellapsed_time(self):
		return time.time() - self.start

	def ellapsed_frames(self):
		pass		

# class Stats(timekeeper):
# 	def __init__(self, max_queue_len = defaults.TIMEKEEPER_HIST_LEN):
# 		super().__init__(max_queue_len)
# 		self.msgs_n = 0

# 	def received_msg(self):
# 		self.msgs_n += 1


# ======  End of TIMEKEEPER  =======
