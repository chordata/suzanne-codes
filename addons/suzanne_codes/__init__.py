# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

bl_info = {
	"name": "Suzanne codes",
	"author": "Bruno Laurencich",
	"version": (0, 1, 0),
	"blender": (2, 80, 0),
	"category": "Development",
}

if "bpy" in locals():
	import importlib
	importlib.reload(ops)
	importlib.reload(gui)
	importlib.reload(props)
	
else:
	from . import ops
	from . import gui
	from . import props
	from . import blend_importer


import bpy


def register():
	ops.register()
	gui.register()
	props.register()

def unregister(): # pragma: no cover
	ops.unregister()
	gui.unregister()
	props.unregister()
