# Suzanne Codes (Blender addon)  
# -- Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the Blender 3D package. Suzanne Codes is largely inspired by Processing.
#
#
# Copyright 2020 Bruno Laurencich
#
# This file is part of Suzanne Codes (Blender addon).
#
# Suzanne Codes (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Suzanne Codes (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import sys
import importlib
import re

from .lib.out import info, debug
from .lib.getters import clean_ob_refs
from .lib import __suzanne_code_close__
from .blend_importer import add_meta_finder, get_sznn_globals
from .blend_importer import handle_exception, clean_error_messages


name = "<user_code>"

def compile_textblock(textblock, name):
    code = textblock.as_string()
    code += "\n\n__suzanne_code_run__( globals() )"

    return compile(code, name, 'exec')


def find_blendfile_imports(code_str): #pragma: no cover (unused)
    matcher = re.compile(r"\W+from\W+blendfile\W+import\W+(\w+)")
    result = []
    for i,line in enumerate(code_str.split("\n")):
        res = matcher.match(line)
        if res:
            result.append((i+1, res.group(1)))

    return result

def get_textblock(context):
    for sp in context.area.spaces:
        if type(sp) == bpy.types.SpaceTextEditor:
            return sp.text


def get_editor(context):
    for sp in context.area.spaces:
        if type(sp) == bpy.types.SpaceTextEditor:
            return sp


# ===============================
# =           GUI OPS           =
# ===============================
  
class SuzanneCode_Run(bpy.types.Operator):
    bl_idname = "suzanne_code.run"
    bl_label = "Suzanne.code Run"

    @classmethod
    def poll(cls, context):
        return not context.window_manager.sznn.modal_running and get_textblock(context) is not None 

    def execute(self, context):
        # textblock_name = "test suzanne run"  
        textblock = get_textblock(context)      
        
        global name
        clean_error_messages(textblock)

        if context.area:
            context.area.tag_redraw()

        try:
            sznn = importlib.import_module(".lib", __package__)
            importlib.reload(sznn)
            
            sznn.__suzanne_code_init__()

            compiled = compile_textblock(textblock, name)

            g = get_sznn_globals(sznn, textblock)

            add_meta_finder(sznn)
            
            exec(compiled, g)

        except Exception as e:
            handle_exception(name, textblock)            
            return {'CANCELLED'}

        return {'FINISHED'}


class SuzanneCode_Stop(bpy.types.Operator):
    bl_idname = "suzanne_code.stop"
    bl_label = "Suzanne.code Stop"

    @classmethod
    def poll(cls, context):
        return context.window_manager.sznn.modal_running

    def execute(self, context):
        context.window_manager.sznn.modal_stop = True
        return {'FINISHED'}

# ======  End of GUI OPS  =======

# ==================================
# =           MAIN MODAL           =
# ==================================



class SuzanneCode_Modal_Op(bpy.types.Operator):
    """"""
    bl_idname = "suzanne_code.modal_op"
    bl_label = "Suzanne.code Modal operator"

    _timer = None

    @classmethod
    def poll(cls, context):
        return get_textblock(context) is not None

    def modal(self, context, event):
        global name
               
        if event.type == 'ESC':
            self.cancel(context)
            return {'CANCELLED'}

        clean_ob_refs()
        
        if event.type == 'TIMER':
            try:
                self.state._set_sznn_state("stage", self.state.SuzanneCodes_STAGES.MODAL)
                self.state._timekeeper.lap()
                res = self.state._modal()
                if res == False or context.window_manager.sznn.modal_stop:
                    self.cancel(context)
                    return {'CANCELLED'}
                
            except Exception as e:
                handle_exception(name, self.textblock)  

                self.cancel(context)
                return {'CANCELLED'}

        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        return self.execute(context)

    def execute(self, context):
        self.state = importlib.import_module(".lib.state", __package__)

        if not self.state._modal or not callable(self.state._modal):
            self.report({'ERROR'}, "SuzanneCode_Modal_Op called without valid state._modal()")
            return {'CANCELLED'}
            
        self.textblock = get_textblock(context) 

        if not self.textblock:
            self.report({'WARNING'}, "No textblock found")
            return {'CANCELLED'}

        fps = context.scene.render.fps
        wm = context.window_manager
        self._timer = wm.event_timer_add(1/fps, window=context.window)

        wm.modal_handler_add(self)

        wm.sznn.modal_running = True
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        wm = context.window_manager
        wm.event_timer_remove(self._timer)
        __suzanne_code_close__()
        wm.sznn.modal_running = False
        wm.sznn.modal_stop = False

        if context.area:
            context.area.tag_redraw()

# ======  End of MAIN MODAL  =======

# ========================================
# =           CHANGE TEXTBLOCK           =
# ========================================


class SuzanneCode_Cycle_Textblocks(bpy.types.Operator):
    bl_idname = "suzanne_code.cycle_textblock"
    bl_label = "Suzanne.code Cycle textblock"

    reverse: bpy.props.BoolProperty(default=False)

    @classmethod
    def poll(cls, context):
        return get_textblock(context) is not None 

    def execute(self, context):
        tbks = list(bpy.data.texts)

        if len(tbks) < 2:
            return {'CANCELLED'}  
        
        tbks.sort(key=lambda t: t.name, reverse=self.reverse)

        editor = get_editor(context)
        tb = get_textblock(context)

        i = tbks.index(tb)
        next_tb = tbks[ (i + 1) % len(tbks) ]

        editor.text = next_tb

        return {'FINISHED'}



# ======  End of CHANGE TEXTBLOCK  =======


def register():
    bpy.utils.register_class(SuzanneCode_Run)
    bpy.utils.register_class(SuzanneCode_Stop)
    bpy.utils.register_class(SuzanneCode_Modal_Op)
    bpy.utils.register_class(SuzanneCode_Cycle_Textblocks)

    # -----------  HOTKEYS  -----------
    
    wm = bpy.context.window_manager
    km = wm.keyconfigs.addon.keymaps.new("Text", space_type='TEXT_EDITOR', region_type='WINDOW')

    if "suzanne_code.run" not in km.keymap_items:
        km.keymap_items.new('suzanne_code.run', 'RET', 'PRESS', ctrl=True)

    if "suzanne_code.cycle_textblock" not in km.keymap_items:
        n = km.keymap_items.new('suzanne_code.cycle_textblock', 'TAB', 'PRESS', ctrl=True)
        n.properties.reverse = False
        r = km.keymap_items.new('suzanne_code.cycle_textblock', 'TAB', 'PRESS', ctrl=True, shift=True)
        r.properties.reverse = True




def unregister():
    bpy.utils.unregister_class(SuzanneCode_Run)
    bpy.utils.unregister_class(SuzanneCode_Stop)
    bpy.utils.unregister_class(SuzanneCode_Modal_Op)
    bpy.utils.unregister_class(SuzanneCode_Cycle_Textblocks)


    wm = bpy.context.window_manager
    km = wm.keyconfigs.addon.keymaps.remove(wm.keyconfigs.addon.keymaps["Text"])





