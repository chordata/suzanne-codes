from __future__ import print_function
import os
import sys
import re
import time
import zipfile

def zip_addon(addon):
	bpy_module = re.sub(".py", "", os.path.basename(os.path.realpath(addon)))
	zfile = os.path.realpath("../" + bpy_module + ".zip")

	print("Zipping addon - {0}".format(bpy_module))

	zf = zipfile.ZipFile(zfile, "w")
	if os.path.isdir(addon):
		for dirname, subdirs, files in os.walk(addon):
			if dirname.endswith("__pycache__"):
				print("OMIT DIR:", dirname)
				continue

			print("ADD DIR:", dirname)

			zf.write(dirname)
			for filename in files:
				if filename.endswith(".pyc"):
					print("OMIT FILE:", filename)
					continue

				print("ADD FILE:", filename)
				zf.write(os.path.join(dirname, filename))
	else:
		zf.write(addon)
	zf.close()
	return (bpy_module, zfile)


zip_addon("suzanne_codes")