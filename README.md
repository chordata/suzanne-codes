# SuzanneCodes Blender Addon

[![pipeline status](https://gitlab.com/chordata/suzanne-codes/badges/master/pipeline.svg)](https://gitlab.com/chordata/suzanne-codes/pipelines) [![coverage report](https://gitlab.com/chordata/suzanne-codes/badges/master/coverage.svg)](https://gitlab.com/chordata/suzanne-codes/-/jobs/artifacts/master/file/tests/htmlcov/index.html?job=coverage)


Flexible software sketchbook-like addon for simplifying the creation of procedural or interactive content within the context of the [Blender 3D package](https://blender.org). Suzanne Codes is largely inspired by [Processing](https://processing.org/).

__This addon is under development, you can get the latest version here:__

[![download_badge](https://img.shields.io/badge/Download%20zip%20-Suzanne%20Codes%20addon-orange)](https://gitlab.com/chordata/suzanne-codes/-/jobs/artifacts/master/raw/suzanne_codes.zip?job=zip)

_This zip file is created from the latest commit in master_


