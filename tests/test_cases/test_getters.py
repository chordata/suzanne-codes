import pytest
import addon_utils
import sys

@pytest.fixture
def OB_Wrapper_cls(sznn, mocker):
	#setup
	OB_Wrapper_cls = sznn.wrap.ob.SuzanneCode_OB
	constructor = OB_Wrapper_cls.__init__
	OB_Wrapper_cls.__init__ = mocker.Mock(return_value=None)

	yield OB_Wrapper_cls

	#teardown
	OB_Wrapper_cls.__init__ = constructor


# ==================================
# =           S FUNCTION           =
# ======================+===========

def test_S(sznn, suzanne_codes_module, sznn_sc, mocker):
	original_get_ob = suzanne_codes_module.lib.getters.get_object
	original_create_ob = suzanne_codes_module.lib.getters.create_object

	getters = suzanne_codes_module.lib.getters

	getters.create_object = mocker.Mock()
	ob = sznn.S("Empty_for_test")

	assert ob.is_created == False
	assert ob.name == "Empty_for_test"
	getters.create_object.assert_not_called()

	getters.get_object = mocker.Mock()
	sznn.S("foo object name")

	getters.get_object.assert_not_called()
	getters.create_object.assert_called_once()

	getters.get_object = original_get_ob
	getters.create_object = original_create_ob

def test_S_diff_primitive(sznn, suzanne_codes_module, empty_sc):
	ob = sznn.S("foo object name", "MONKEY")
	ob2 = sznn.S("foo object name", "PLANE")

	assert ob != ob2
	assert ob.name.startswith("foo object name")
	assert ob2.name.startswith("foo object name")

	assert len(ob.data.vertices) != len(ob2.data.vertices) 


def test_S_diff_transform(sznn, suzanne_codes_module, empty_sc, random_loc):
	ob = sznn.S("foo object name", location = random_loc)
	ob2 = sznn.S("foo object name", location = random_loc.zyx)

	assert ob == ob2
	assert ob.location == random_loc.zyx




# ======  End of S FUNCTION  =======


# =====================================
# =           CREATE OBJECT           =
# =====================================


def test_create_object(bpy_test, sznn, empty_sc):
	assert len(bpy_test.data.objects) == 0
	assert len(bpy_test.data.meshes) == 0
	assert "sznn: objects" not in bpy_test.data.collections

	ob = sznn.create_object()
	assert type(ob) == sznn.wrap.ob.SuzanneCode_OB
	assert ob == bpy_test.data.objects[0] 
	assert "sznn: objects" in bpy_test.data.collections
	assert ob.type == 'MESH'

	ob2 = sznn.create_object()

	assert ob != ob2


def test_create_empty(bpy_test, sznn, empty_sc):
	e = sznn.create_empty()
	assert e.type == 'EMPTY'


def test_create_custom_mesh(bpy_test, sznn, empty_sc):
	m = bpy_test.data.meshes.new("foo_mesh")
	ob = sznn.create_object(mesh = m)
	assert ob.data == m
	assert ob._primitive == 'CUSTOM'


def test_create_wrong_arg(bpy_test, sznn, empty_sc):
	with pytest.raises(TypeError):
		sznn.create_object(name = 12)

	with pytest.raises(TypeError):
		sznn.create_object(mesh = 12)

	with pytest.raises(KeyError):
		sznn.create_object(mesh = "HYPERCUBE")		


def test_cache_create_object(bpy_test, sznn, empty_sc):
	ob = sznn.create_object()

	assert ob.name in sznn.state._get_sznn_state("ob_cache")
	assert ob == sznn.state._get_sznn_state("ob_cache")[ob.name]


def test_remove_created_objects(bpy_test, sznn, empty_sc):
	ob = sznn.create_object()
	ob_name = ob.name[:]
	assert ob.is_created == True

	obE = bpy_test.data.objects.new( "this_one_should_remain" , None )

	sznn.remove_created_objects()

	assert ob_name not in bpy_test.data.objects
	assert obE.name in bpy_test.data.objects

	with pytest.raises(NameError, match=r".*perhaps it was removed or renamed"):
		ob.rotation #<-- asking for any prop should raise


def test_create_object_location(bpy_test, sznn, empty_sc, random_loc):
	ob = sznn.create_object(location = random_loc)

	assert ob.location == random_loc


def test_create_object_rotation(bpy_test, sznn, empty_sc, random_rot):
	ob = sznn.create_object(rotation = random_rot)

	assert ob.rotation == random_rot


def test_create_object_scale(bpy_test, sznn, empty_sc, random_scale):
	ob = sznn.create_object(scale = random_scale)

	assert ob.scale == random_scale

def test_create_object_parent(bpy_test, sznn, empty_sc, random_loc):
	obP = sznn.create_object()

	ob = sznn.create_object(location = random_loc, parent=obP)

	parent_move = random_loc.zyx
	obP.set_location(parent_move)

	assert ob.parent == obP

	ob.evaluate(bpy_test.context)

	assert ob.matrix.to_translation() == parent_move + random_loc


# ======  End of CREATE OBJECT  =======


# ==================================
# =           GET OBJECT           =
# ==================================

def test_get_ob(sznn, sznn_sc):
	ob = sznn.get_object("Empty_for_test")
	assert ob.name == "Empty_for_test"
	assert ob.is_created == False


def test_recreate_reference(sznn, sznn_sc):
	ob = sznn.get_object("Empty_for_test")
	assert ob.name == "Empty_for_test"

	raw = ob.raw

	ob._ob = None

	assert raw == ob.raw 

def test_cache_get_object_str(sznn, sznn_ob, OB_Wrapper_cls):
	assert sznn_ob.name in sznn.state._get_sznn_state("ob_cache")
	assert sznn_ob == sznn.state._get_sznn_state("ob_cache")[sznn_ob.name]

	ob1 = sznn_ob

	ob2 = sznn.get_object("Empty_for_test")

	OB_Wrapper_cls.__init__.assert_not_called()

	assert ob1 == ob2


def test_cache_get_object_bpyOB(sznn, sznn_ob, OB_Wrapper_cls):
	ob1 = sznn_ob

	ob2 = sznn.get_object(ob1.raw)

	OB_Wrapper_cls.__init__.assert_not_called()

	assert ob1 == ob2


def test_cache_get_object_wrapper(sznn, sznn_ob, OB_Wrapper_cls):
	ob1 = sznn_ob

	ob2 = sznn.get_object(ob1)

	OB_Wrapper_cls.__init__.assert_not_called()

	assert ob1 == ob2


def test_noncache_get_object_wrapper(sznn, sznn_ob):
	sznn._reset_sznn_state()
	
	ob2 = sznn.get_object(sznn_ob)

	assert sznn_ob == ob2

def test_noncache_get_object_bpyOB(sznn, sznn_ob):
	sznn._reset_sznn_state()
	
	ob2 = sznn.get_object(sznn_ob.raw)

	assert sznn_ob == ob2





# ======  End of GET OBJECT  =======
