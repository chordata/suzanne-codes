import pytest
import rot_utils
import mathutils

import math

def test_scale_props(scale_ob, random_scale, scale_one):
	scale_ob.scale.x = random_scale.x
	scale_ob.scale.y = random_scale.y
	scale_ob.scale.z = random_scale.z

	assert scale_ob.scale != scale_one
	assert scale_ob.scale == random_scale

# -----------  SET SCALE  -----------

def test_scale_set_vector(scale_ob, random_scale):
	scale_ob.set_scale(scale=random_scale)
	assert scale_ob.scale == random_scale

def test_scale_set_list(scale_ob, random_scale):
	scale_ob.set_scale(scale=list(random_scale))
	assert scale_ob.scale == random_scale

def test_scale_set_XYZ(sznn, scale_ob, random_scale):
	tup = tuple(random_scale)
	scale_ob.set_scale(tup[0], tup[1], tup[2])
	assert scale_ob.scale == random_scale

def test_scale_set_Z(sznn, scale_ob, random_scale):
	tup = tuple(random_scale)
	scale_ob.set_scale(z = tup[2])
	assert tuple(scale_ob.scale) == (1, 1, random_scale.z)

def test_scale_set_Y(sznn, scale_ob, random_scale):
	tup = tuple(random_scale)
	scale_ob.set_scale(y = tup[1])
	assert tuple(scale_ob.scale) == (1, random_scale.y, 1)

def test_scale_set_D(sznn, scale_ob, random_scale):
	tup = tuple(random_scale)
	scale_ob.set_scaleX(tup[0])
	scale_ob.set_scaleY(tup[1])
	scale_ob.set_scaleZ(tup[2])
	assert scale_ob.scale == random_scale

def test_scale_set_matrix(scale_ob, random_scale):
	factor = random_scale.x
	matrix = mathutils.Matrix().Scale(factor, 4)
	scale_ob.set_scale(matrix)
	assert (factor,)*3 == tuple(scale_ob.scale)

# # -----------  CHANGE SCALE  -----------

def test_change_scale_vector(scale_ob, random_scale, scale_one):
	scale_ob.change_scale(scale=random_scale)
	assert scale_ob.scale == (random_scale + scale_one)

def test_change_scale_XYZ(sznn, scale_ob, random_scale, scale_one):
	tup = tuple(random_scale)
	scale_ob.change_scale(tup[0], tup[1], tup[2])
	assert scale_ob.scale == (random_scale + scale_one)

def test_change_scale_Z(sznn, scale_ob, random_scale, scale_one):
	tup = tuple(random_scale)
	scale_ob.change_scale(z = tup[2])
	assert tuple(scale_ob.scale) == pytest.approx((1, 1, random_scale.z + 1), abs = 0.001)

def test_change_scale_Y(sznn, scale_ob, random_scale, scale_one):
	tup = tuple(random_scale)
	scale_ob.change_scale(y = tup[1])
	assert tuple(scale_ob.scale) == pytest.approx((1, random_scale.y + 1, 1), abs = 0.001)

def test_change_scale_D(sznn, scale_ob, random_scale, scale_one):
	tup = tuple(random_scale)
	scale_ob.change_scaleX(tup[0])
	scale_ob.change_scaleY(tup[1])
	scale_ob.change_scaleZ(tup[2])
	assert scale_ob.scale == (random_scale + scale_one)

def test_change_scale_matrix(scale_ob, random_scale, scale_one):
	#set an arbritrary scale to test the change later
	scale_ob.set_scale(8,8,8)
	factor = random_scale.x
	matrix = mathutils.Matrix().Scale(factor, 4)
	scale_ob.change_scale(matrix)
	assert (factor+8,)*3 == pytest.approx(tuple(scale_ob.scale), abs = 0.001)

