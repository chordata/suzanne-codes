import pytest

import mathutils
import math

# =================================
# =           ROT UTILS           =
# =================================

def get_rots_as_quats(rot_a, rot_b):
	if isinstance(rot_a, (mathutils.Euler, mathutils.Matrix)):
		quat_a = rot_a.to_quaternion()
	elif isinstance(rot_a, mathutils.Quaternion):
		quat_a = rot_a
	else:
		raise TypeError("The value {} is not a valid rotation".format(rot_a))


	if isinstance(rot_b, (mathutils.Euler, mathutils.Matrix)):
		quat_b = rot_b.to_quaternion()
	elif isinstance(rot_b, mathutils.Quaternion):
		quat_b = rot_b
	else:
		raise TypeError("The value {} is not a valid rotation".format(rot_b))
	
	return (quat_a, quat_b)	

def are_same_rot(rot_a, rot_b):
	"""A given rotation can be represented by several Euler values, so in order to test them 
	they are transformed to quaternion and the dot product is used to get the diff btw them"""

	quat_a, quat_b = get_rots_as_quats(rot_a, rot_b)

	diff = quat_a.dot(quat_b)
	return abs(diff) == pytest.approx(1)


def get_rot_diff_quat(rot_a, rot_b):
	quat_a, quat_b = get_rots_as_quats(rot_a, rot_b)

	rot_diff = quat_a.rotation_difference(quat_b)

	return rot_diff

# ======  End of ROT UTILS  =======