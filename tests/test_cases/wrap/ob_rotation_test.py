import pytest
import rot_utils
import mathutils
import math

def test_rotation_props(rot_ob, random_rot, zero_rot):
	rot_ob.rotation.x = random_rot.x
	rot_ob.rotation.y = random_rot.y
	rot_ob.rotation.z = random_rot.z

	assert rot_ob.rotation != zero_rot
	assert rot_ob.rotation_euler == random_rot
	assert rot_ob.rotation_quaternion == random_rot.to_quaternion()

# -----------  SET ROTATION  -----------

def test_rotation_set_euler(rot_ob, random_rot):
	rot_ob.set_rotation(rotation=random_rot)
	assert rot_ob.rotation == random_rot

def test_rotation_set_XYZ(sznn, rot_ob, random_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.set_rotation(tup[0], tup[1], tup[2])
	assert rot_ob.rotation == random_rot

def test_rotation_set_Z(sznn, rot_ob, random_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.set_rotation(z = tup[2])
	assert tuple(rot_ob.rotation) == (0, 0, random_rot.z)

def test_rotation_set_Y(sznn, rot_ob, random_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.set_rotation(y = tup[1])
	assert tuple(rot_ob.rotation) == (0, random_rot.y, 0)

def test_rotation_set_D(sznn, rot_ob, random_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.set_rotationX(tup[0])
	rot_ob.set_rotationY(tup[1])
	rot_ob.set_rotationZ(tup[2])

	assert rot_ob.rotation == random_rot

def test_rotation_set_matrix(rot_ob, random_rot):
	matrix = random_rot.to_matrix()
	rot_ob.set_rotation(matrix)

	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

def test_rotation_set_quat(rot_ob, random_rot):
	quat = random_rot.to_quaternion()
	rot_ob.set_rotation(quat)

	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

# -----------  ROTATE  -----------


def test_rotate_euler(rot_ob, random_rot, zero_rot):
	rot_ob.rotate(random_rot)
	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	rot_diff = rot_utils.get_rot_diff_quat(random_rot, zero_rot)
	rot_ob.rotate(rot_diff.to_euler())

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)

def test_rotate_euler_tuple(sznn, rot_ob, random_rot, zero_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.rotate(tup)

	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	rot_diff = rot_utils.get_rot_diff_quat(random_rot, zero_rot)
	rot_ob.rotate(rot_diff)

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)
	

def test_rotate_quat(rot_ob, random_rot, zero_rot):
	rot_ob.rotate(random_rot.to_quaternion())
	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	rot_diff = rot_utils.get_rot_diff_quat(random_rot, zero_rot)
	rot_ob.rotate(rot_diff)

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)

def test_rotate_quat_tuple(rot_ob, random_rot, zero_rot):
	rot_ob.rotate(tuple(random_rot.to_quaternion()))
	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	rot_diff = rot_utils.get_rot_diff_quat(random_rot, zero_rot)
	rot_ob.rotate(rot_diff)

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)
	

def test_rotate_matrix(rot_ob, random_rot, zero_rot):
	rot_ob.rotate(random_rot.to_matrix())
	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	rot_diff = rot_utils.get_rot_diff_quat(random_rot, zero_rot)
	rot_ob.rotate(rot_diff.to_matrix())

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)
	

def test_rotate_XYZ(sznn, rot_ob, random_rot, zero_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.rotate(tup[0], tup[1], tup[2])

	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	rot_diff = rot_utils.get_rot_diff_quat(random_rot, zero_rot)
	rot_ob.rotate(rot_diff)

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)


def test_rotate_XY(sznn, rot_ob, random_rot, zero_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.rotate(x = tup[0], z = tup[2])

	random_rot.y = 0

	assert rot_utils.are_same_rot(rot_ob.rotation, random_rot)

	# In ordet to obtain the opposite rotation we have to rotate the single 
	# elements in reverse order
	rot_ob.rotate(z = -tup[2])
	rot_ob.rotate(x = -tup[0])

	assert rot_utils.are_same_rot(rot_ob.rotation, zero_rot)

def test_rotate_D(sznn, rot_ob, random_rot):
	tup = sznn.rad_to_deg(tuple(random_rot))
	rot_ob.rotateX(tup[0])
	rot_ob.rotateY(tup[1])
	rot_ob.rotateZ(tup[2])

	assert rot_ob.rotation == random_rot


# -----------  LOOK AT  -----------

def test_look_atVector(rot_ob, random_loc, zero_rot, mocker):
	rot_ob.look_at(random_loc)
	assert not rot_utils.are_same_rot(rot_ob.rotation, zero_rot)

def test_look_atXYZ(rot_ob, random_loc, zero_rot, mocker):
	rot_ob.look_at(random_loc.x, random_loc.y, random_loc.z)
	assert not rot_utils.are_same_rot(rot_ob.rotation, zero_rot)

def test_look_at_keyword_args(rot_ob, random_loc, mocker):
	rot_ob._generic_var_vec_handler = mocker.Mock()
	any_args = (mocker.ANY, ) *4

	rot_ob.look_at(random_loc)
	rot_ob._generic_var_vec_handler.assert_called_with(*any_args, ["-Y", "Z"])

	rot_ob._generic_var_vec_handler.reset_mock()

	rot_ob.look_at(random_loc, direction = "X")
	rot_ob._generic_var_vec_handler.assert_called_with(*any_args, ["X", "Z"])

	rot_ob._generic_var_vec_handler.reset_mock()

	rot_ob.look_at(random_loc, direction = "Y", up="X")
	rot_ob._generic_var_vec_handler.assert_called_with(*any_args, ["Y", "X"])

	with pytest.raises(TypeError):
		rot_ob.look_at(random_loc,"X")		

	with pytest.raises(TypeError):
		rot_ob.look_at(random_loc.x, random_loc.y, random_loc.z, "-Y")		