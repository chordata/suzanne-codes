import pytest


def test_wrap_ob_eq(sznn_two_obs, sznn_get_object):
	ob1, ob2 = sznn_two_obs

	name_ob1 = ob1.name
	name_ob2 = ob2.name

	ob1b = sznn_get_object(name_ob1)
	ob2b = sznn_get_object(name_ob2)

	assert ob1 !=  ob1.name
	assert ob1 ==  ob1b
	assert ob2 ==  ob2b

	assert ob1 == ob1.raw
	assert ob1 != ob2.raw

	assert hash(ob1) == hash(ob1.raw)


def test_wrap_ob_init_raw_ob(sznn_ob, sznn_get_object):
	ob = sznn_get_object(sznn_ob.raw)
	assert sznn_ob.raw == ob.raw 

def test_wrap_ob_init_copy(sznn_ob, sznn_get_object):
	ob = sznn_get_object(sznn_ob)
	assert sznn_ob.raw == ob.raw 

def test_wrap_ob_warning_quat_rot(sznn, sznn_ob, sznn_get_object, mocker):
	sznn._reset_sznn_state() #clear the cache created at the sznn_ob fixture

	sznn_ob.raw.rotation_mode = 'QUATERNION'
	sznn.out.warning = mocker.Mock()	

	ob = sznn_get_object("Empty_for_test")
	ob = sznn_get_object(ob)

	sznn.out.warning.assert_called_once()	


def test_wrap_ob_init_wrong_type(sznn_ob, sznn_get_object):
	#passing a list of objects 
	with pytest.raises(TypeError):
		sznn_get_object([sznn_ob.name])

def test_wrap_ob_init_not_found(sznn_ob, sznn_get_object):
	with pytest.raises(NameError):
		sznn_get_object("this object will for sure not belong to the scene")


def test_wrap_ob_init_not_in_scene(bpy_data, sznn_ob, sznn_get_object):
	ob = bpy_data.objects.new("Empty_unlinked", None)
	with pytest.raises(NameError):
		sznn_get_object(ob)

	with pytest.raises(NameError):
		sznn_get_object(ob.name)

	bpy_data.objects.remove(ob)


def test_wrap_ob_data_mesh(sznn_ob, sznn_get_object):
	assert sznn_ob.data == sznn_ob.raw.data 

# -----------  Parent  -----------

def test_wrap_ob_set_parent_wrap(sznn, sznn_ob):
	ob = sznn.get_object(sznn_ob.raw)
	assert sznn_ob.parent == ob.parent 

	ob2 = sznn.create_object()

	ob.set_parent(ob2)

	assert ob.parent == ob2


def test_wrap_ob_set_parent_wrong(sznn, sznn_ob):
	with pytest.raises(TypeError):
		sznn_ob.set_parent("another_object's_name")

def test_wrap_ob_set_parent_rawOb(sznn, sznn_ob):
	ob = sznn.get_object(sznn_ob.raw)
	assert sznn_ob.parent == ob.parent 

	ob2 = sznn.create_object()

	ob.set_parent(ob2.raw)

	assert ob.parent == ob2


def test_wrap_ob_set_raw_parent(sznn, sznn_ob):
	ob = sznn.get_object(sznn_ob.raw)
	assert sznn_ob.parent == ob.parent 

	ob2 = sznn.create_object()

	ob.raw.parent = ob2.raw

	assert ob.parent == ob2


def test_wrap_ob_get_with_raw_parent(bpy_test, sznn, empty_sc):

	ob1 = bpy_test.data.objects.new( "Empty1" , None )
	bpy_test.context.scene.collection.objects.link(ob1)
	ob2 = bpy_test.data.objects.new( "Empty2" , None )
	bpy_test.context.scene.collection.objects.link(ob2)
	ob3 = bpy_test.data.objects.new( "Empty2" , None )
	bpy_test.context.scene.collection.objects.link(ob3)

	ob1.parent = ob2

	obw1 = sznn.get_object(ob1)
	obw2 = sznn.get_object(ob2)
	
	assert obw1.parent == obw2 

	obw1.raw.parent = ob3

	assert obw1.parent == sznn.get_object(ob3)