import pytest
import rot_utils

def test_arg_kwargs_type_wrong(sznn_ob, random_rot):

	with pytest.raises(TypeError, match='No arguments found'):
		sznn_ob.move()

	with pytest.raises(TypeError, match='No arguments found'):
		sznn_ob.rotate()

	with pytest.raises(TypeError, match='Expected 3.*or 1.*arguments'):
		sznn_ob.rotate(1,2,3,4)

	with pytest.raises(TypeError, match='Expected 3.*or 1.*arguments'):
		sznn_ob.move(1,2,3,4)

	with pytest.raises(TypeError, match='Cannot mix positional.*and keyword arguments'):
		sznn_ob.rotate((1,2,3), rotation=(1,2,3))
		
	with pytest.raises(TypeError, match='Cannot mix positional.*and keyword arguments'):
		sznn_ob.move((1,2,3), location=(1,2,3))

	with pytest.raises(TypeError, match='The keyword argument.*is not valid'):
		sznn_ob.rotate(foo=(1,2,3))
		
	with pytest.raises(TypeError, match='The keyword argument.*is not valid'):
		sznn_ob.move(foo=(1,2,3))

	with pytest.raises(TypeError, match='The key.*cannot be used togheter with'):
		sznn_ob.rotate(x=1, y=2, rotation=(1,2,3))
		
	with pytest.raises(TypeError, match='The key.*cannot be used togheter with'):
		sznn_ob.move(x=1, y=2, location=(1,2,3))

	# sznn_ob.rotate(1,2)


def test_arg_euler_from_single(sznn_ob, random_rot):
	with pytest.raises(TypeError, match='doesn\'t represent a valid rotation'):
		sznn_ob.rotate(rotation=12)

	with pytest.raises(TypeError, match='doesn\'t represent a valid location'):
		sznn_ob.move(location=12)

	with pytest.raises(TypeError, match='doesn\'t represent a valid scale'):
		sznn_ob.set_scale(scale=12)

	with pytest.raises(TypeError, match='doesn\'t represent a valid rotation'):
		sznn_ob.rotate(rotation=(1,2,3,4,5))

	with pytest.raises(TypeError, match='doesn\'t represent a valid location'):
		sznn_ob.set_location((1,2,3,4,5))

	with pytest.raises(TypeError, match='doesn\'t represent a valid scale'):
		sznn_ob.change_scale((1,2,3,4,5))
