import pytest
import rot_utils
import mathutils


import math

def test_location_props(loc_ob, random_loc, zero_loc):
	loc_ob.location.x = random_loc.x
	loc_ob.location.y = random_loc.y
	loc_ob.location.z = random_loc.z

	assert loc_ob.location != zero_loc
	assert loc_ob.location == random_loc

# -----------  SET LOCATION  -----------

def test_location_set_vector(loc_ob, random_loc):
	loc_ob.set_location(location=random_loc)
	assert loc_ob.location == random_loc

def test_location_set_XYZ(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.set_location(tup[0], tup[1], tup[2])
	assert loc_ob.location == random_loc

def test_location_set_Z(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.set_location(z = tup[2])
	assert tuple(loc_ob.location) == (0, 0, random_loc.z)

def test_location_set_Y(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.set_location(y = tup[1])
	assert tuple(loc_ob.location) == (0, random_loc.y, 0)

def test_location_set_D(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.set_locationX(tup[0])
	loc_ob.set_locationY(tup[1])
	loc_ob.set_locationZ(tup[2])
	assert loc_ob.location == random_loc

def test_location_set_matrix(loc_ob, random_loc):
	matrix = mathutils.Matrix().Translation(random_loc)
	loc_ob.set_location(matrix)
	assert random_loc == loc_ob.location

# # -----------  MOVE  -----------

def test_move_vector(loc_ob, random_loc):
	loc_ob.move(location=random_loc)
	assert loc_ob.location == random_loc

def test_move_tuple(loc_ob, random_loc):
	loc_ob.move(location=tuple(random_loc))
	assert loc_ob.location == random_loc

def test_move_XYZ(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.move(tup[0], tup[1], tup[2])
	assert loc_ob.location == random_loc

def test_move_Z(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.move(z = tup[2])
	assert tuple(loc_ob.location) == (0, 0, random_loc.z)

def test_move_Y(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.move(y = tup[1])
	assert tuple(loc_ob.location) == (0, random_loc.y, 0)

def test_move_D(sznn, loc_ob, random_loc):
	tup = tuple(random_loc)
	loc_ob.moveX(tup[0])
	loc_ob.moveY(tup[1])
	loc_ob.moveZ(tup[2])
	assert loc_ob.location == random_loc

def test_move_matrix(loc_ob, random_loc):
	matrix = mathutils.Matrix().Translation(random_loc)
	loc_ob.move(matrix)
	assert random_loc == loc_ob.location

# -----------  GO TOWARDS  -----------
		

def test_go_towards(loc_ob, random_loc):
	initial_loc = loc_ob.location.copy()
	distance = (random_loc - loc_ob.location).magnitude
	loc_ob.go_towards(location=random_loc, speed= distance/2)
	assert loc_ob.location != random_loc
	assert loc_ob.location != initial_loc

	tup = tuple(random_loc)
	loc_ob.go_towards(*tup, speed= distance/2)
	assert loc_ob.location == random_loc

	loc_ob.go_towards( -initial_loc.x, -initial_loc.y, -initial_loc.z, distance)
	assert tuple(loc_ob.location) == pytest.approx(tuple(initial_loc),  abs=0.001)


def test_go_towardsXYZ(sznn, loc_ob, random_loc):
	initial_loc = loc_ob.location.copy()
	loc_ob.go_towards(x=random_loc.x, speed= abs(random_loc.x - initial_loc.x))

	test_partial_loc = (random_loc.x,0,0)
	assert test_partial_loc == pytest.approx(tuple(loc_ob.location), abs=0.001)

	loc_ob.go_towards(y=random_loc.y, speed= abs(random_loc.y - initial_loc.x))

	test_partial_loc = (random_loc.x, random_loc.y, 0)
	assert test_partial_loc == pytest.approx(tuple(loc_ob.location), abs=0.001)

	loc_ob.go_towards(z=random_loc.z, speed= abs(random_loc.z - initial_loc.x))

	test_partial_loc = (random_loc.x, random_loc.y, random_loc.z)
	assert test_partial_loc == pytest.approx(tuple(loc_ob.location), abs=0.001)

def test_go_towards_limit_speed(loc_ob, random_loc):
	initial_loc = loc_ob.location.copy()
	distance = (random_loc - loc_ob.location).magnitude
	loc_ob.go_towards(location=random_loc, speed= distance*2)
	assert loc_ob.location == random_loc

def test_go_towards_default_speed(loc_ob, random_loc):
	initial_loc = loc_ob.location.copy()
	distance = (random_loc - loc_ob.location).magnitude
	loc_ob.go_towards(location=random_loc)
	
	delta_pos = loc_ob.location - initial_loc

	assert delta_pos.magnitude == pytest.approx(loc_ob._go_towards_speed_def, abs=0.001)





# def test_move_Y(sznn, loc_ob, random_loc):
# 	tup = tuple(random_loc)
# 	loc_ob.go_towards(y = tup[1])
# 	assert tuple(loc_ob.location) == (0, random_loc.y, 0)


# def test_go_towards_vector(loc_ob, random_loc):
# 	initial_loc = loc_ob.location.copy()
# 	distance = (random_loc - loc_ob.location).magnitude
# 	loc_ob.go_towards(location=random_loc, speed= distance/2)
# 	assert loc_ob.location != random_loc
# 	assert loc_ob.location != initial_loc

# 	loc_ob.go_towards(location=random_loc, speed= distance/2)
# 	assert loc_ob.location == random_loc

# def test_rotate_euler(loc_ob, random_loc, zero_loc):
# 	loc_ob.rotate(random_loc)
# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	rot_diff = rot_utils.get_rot_diff_quat(random_loc, zero_loc)
# 	loc_ob.rotate(rot_diff.to_euler())

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)

# def test_rotate_euler_tuple(sznn, loc_ob, random_loc, zero_loc):
# 	tup = sznn.rad_to_deg(tuple(random_loc))
# 	loc_ob.rotate(tup)

# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	rot_diff = rot_utils.get_rot_diff_quat(random_loc, zero_loc)
# 	loc_ob.rotate(rot_diff)

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)
	

# def test_rotate_quat(loc_ob, random_loc, zero_loc):
# 	loc_ob.rotate(random_loc.to_quaternion())
# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	rot_diff = rot_utils.get_rot_diff_quat(random_loc, zero_loc)
# 	loc_ob.rotate(rot_diff)

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)

# def test_rotate_quat_tuple(loc_ob, random_loc, zero_loc):
# 	loc_ob.rotate(tuple(random_loc.to_quaternion()))
# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	rot_diff = rot_utils.get_rot_diff_quat(random_loc, zero_loc)
# 	loc_ob.rotate(rot_diff)

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)
	

# def test_rotate_matrix(loc_ob, random_loc, zero_loc):
# 	loc_ob.rotate(random_loc.to_matrix())
# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	rot_diff = rot_utils.get_rot_diff_quat(random_loc, zero_loc)
# 	loc_ob.rotate(rot_diff.to_matrix())

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)
	

# def test_rotate_XYZ(sznn, loc_ob, random_loc, zero_loc):
# 	tup = sznn.rad_to_deg(tuple(random_loc))
# 	loc_ob.rotate(tup[0], tup[1], tup[2])

# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	rot_diff = rot_utils.get_rot_diff_quat(random_loc, zero_loc)
# 	loc_ob.rotate(rot_diff)

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)


# def test_rotate_XY(sznn, loc_ob, random_loc, zero_loc):
# 	tup = sznn.rad_to_deg(tuple(random_loc))
# 	loc_ob.rotate(x = tup[0], z = tup[2])

# 	random_loc.y = 0

# 	assert rot_utils.are_same_rot(loc_ob.location, random_loc)

# 	# In ordet to obtain the opposite location we have to rotate the single 
# 	# elements in reverse order
# 	loc_ob.rotate(z = -tup[2])
# 	loc_ob.rotate(x = -tup[0])

# 	assert rot_utils.are_same_rot(loc_ob.location, zero_loc)

# def test_rotate_D(sznn, loc_ob, random_loc):
# 	tup = sznn.rad_to_deg(tuple(random_loc))
# 	loc_ob.rotateX(tup[0])
# 	loc_ob.rotateY(tup[1])
# 	loc_ob.rotateZ(tup[2])

# 	assert loc_ob.location == random_loc
