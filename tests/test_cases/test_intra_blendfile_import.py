import pytest
import sys
from os.path import dirname
from os.path import join
import importlib
import mathutils
import re

MODULES_DIR = join(dirname(__file__), "fake_modules")


def _read_mod_file(filename):
	file = join(MODULES_DIR, filename)
	with open(file) as f:
		return f.read()

@pytest.fixture
def code_sample_main():
	return _read_mod_file("main_script.py")

@pytest.fixture
def code_sample_mod_a():
	return _read_mod_file("module_a.py")

@pytest.fixture
def code_sample_mod_b():
	return _read_mod_file("module_b.py")


@pytest.fixture
def blend_with_texts(bpy_test, code_sample_main, code_sample_mod_a, code_sample_mod_b):
	main = bpy_test.data.texts.new("code_sample_main")
	main.from_string(code_sample_main)
	
	mod_a = bpy_test.data.texts.new("module_a.py")
	mod_a.from_string(code_sample_mod_a)

	mod_b = bpy_test.data.texts.new("module_b")
	mod_b.from_string(code_sample_mod_b)

	yield (main, mod_a, mod_b)

	for t in bpy_test.data.texts:
		bpy_test.data.texts.remove(t)


@pytest.fixture
def blend_with_texts_errors(bpy_test, blend_with_texts):
	syntax_error = bpy_test.data.texts.new("module_syntax_error")
	syntax_error.from_string( _read_mod_file("module_syntax_error.py") )

	name_error = bpy_test.data.texts.new("module_name_error")
	name_error.from_string( _read_mod_file("module_name_error.py") )

	yield (syntax_error, name_error)


@pytest.fixture
def add_module_finder(suzanne_codes_module, sznn):
	# original_path = sys.meta_path.copy()

	finder = suzanne_codes_module.blend_importer.add_meta_finder(sznn)

	yield finder
	# sys.meta_path = original_path
	importlib.invalidate_caches()
	suzanne_codes_module.blend_importer.clean_sys_modules()


def test_blend_module_find(blend_with_texts, add_module_finder):
	import module_a
	assert module_a.who_am_i == "module_a"
	assert module_a.__file__.endswith("module_a.py")

	from module_b import Module_B_Class
	instance = Module_B_Class()

	assert instance.which_mod_am_i() == "module_b"


def test_blend_modules_conflict(bpy_test, suzanne_codes_module, blend_with_texts, add_module_finder, mocker):
	mod_b = bpy_test.data.texts.new("module_b.py")
	mod_b.from_string("#this should not be imported")

	def raise_original_exception(*args):
	    exc_type, exc_obj, tb = sys.exc_info()
	    raise exc_obj

	original_handle_exception = suzanne_codes_module.blend_importer.handle_exception
	suzanne_codes_module.blend_importer.handle_exception = mocker.Mock(side_effect = raise_original_exception)

	with pytest.raises(ImportError):
		import module_a

	suzanne_codes_module.blend_importer.handle_exception = original_handle_exception


def test_blend_module_reload(bpy_test, blend_with_texts, add_module_finder):
	import module_b
	assert not hasattr(module_b, "foo")
	
	code = bpy_test.data.texts["module_b"].as_string()
	code += "\nfoo = 23"
	bpy_test.data.texts["module_b"].from_string(code)

	importlib.reload(module_b)
	assert module_b.foo == 23

def test_blend_module_sznn_props(sznn, blend_with_texts, add_module_finder):
	import module_a
	assert module_a.sznn == sznn
	assert module_a.Vector == mathutils.Vector


def test_get_sznn_globals(sznn, blend_with_texts, suzanne_codes_module):
	get_sznn_globals = suzanne_codes_module.blend_importer.get_sznn_globals

	g = get_sznn_globals(sznn, textblock = blend_with_texts[0])

	assert blend_with_texts[0].name in g["__file__"]
	

# =====================================
# =           IMPORT ERRORS           =
# =====================================

@pytest.fixture
def mock_info_fn(suzanne_codes_module, mocker):
	original_info = suzanne_codes_module.blend_importer.info

	output = []

	def register_output(*args):
		nonlocal output
		output.append(args)

	suzanne_codes_module.blend_importer.info = mocker.Mock(side_effect = register_output)

	yield  suzanne_codes_module.blend_importer.info, output
	#teardown
	suzanne_codes_module.blend_importer.info = original_info 


def test_blend_module_syntax_error(suzanne_codes_module, blend_with_texts_errors, add_module_finder, mock_info_fn):
	info_fn, output = mock_info_fn
	syntax_error_textblock = blend_with_texts_errors[0]

	import module_syntax_error

	info_fn.assert_called()

	correct_line = False
	for line in output:
		if type(line[0]) == str:
			if re.match(r"Syntax Error\W+.+line:.+{}".format(14), line[0]):
				correct_line = True
				break

	assert correct_line #an error message was printed with the correct error line


	correct_file = False
	for line in output:
		if type(line[0]) == str:
			if re.match(r"Syntax Error\W+on.+{}".format("module_syntax_error"), line[0]):
				correct_file = True
				break

	assert correct_file #an error message was printed with the correct textblock name

	mark_in_code = False
	for line in syntax_error_textblock.lines:
		if re.match(r"[^#]+#\W<<<\WSYNTAX ERROR", line.body):
			mark_in_code = True
			break

	assert mark_in_code # the source script has been marked with a comment


def test_blend_module_syntax_error_indirect(bpy_test, suzanne_codes_module, blend_with_texts_errors, add_module_finder, mock_info_fn):
	info_fn, output = mock_info_fn
	syntax_error_textblock = blend_with_texts_errors[0]

	mod_i = bpy_test.data.texts.new("indirect_import")
	mod_i.from_string("import module_syntax_error as mm\nmm.fun() ")

	import indirect_import

	info_fn.assert_called()

	correct_line = False
	for line in output:
		if type(line[0]) == str:
			if re.match(r"Syntax Error\W+.+line:.+{}".format(14), line[0]):
				correct_line = True
				break

	assert correct_line #an error message was printed with the correct error line


	correct_file = False
	for line in output:
		if type(line[0]) == str:
			if re.match(r"Syntax Error\W+on.+{}".format("module_syntax_error"), line[0]):
				correct_file = True
				break

	assert correct_file #an error message was printed with the correct textblock name

	mark_in_code = False
	for line in mod_i.lines:
		if re.match(r"[^#]+#\W<<<.*ERROR", line.body):
			mark_in_code = True
			break

	assert mark_in_code # the main script has been marked with a comment

	mark_in_code = False
	for line in syntax_error_textblock.lines:
		if re.match(r"[^#]+#\W<<<\WSYNTAX ERROR", line.body):
			mark_in_code = True
			break

	assert mark_in_code # the imported script has been marked with a comment


def test_blend_module_name_error_indirect(bpy_test, suzanne_codes_module, blend_with_texts_errors, add_module_finder, mock_info_fn):
	info_fn, output = mock_info_fn
	name_error_textblock = blend_with_texts_errors[1]

	mod_i = bpy_test.data.texts.new("indirect_import")
	mod_i.from_string("import module_name_error as mm\nmm.fun() ")

	import indirect_import

	info_fn.assert_called()

	correct_line = False
	for line in output:
		if type(line[0]) == str:
			if re.match(r"Error\W+.+line:.+{}".format(14), line[0]):
				correct_line = True
				break

	assert correct_line #an error message was printed with the correct error line


	correct_file = False
	for line in output:
		if type(line[0]) == str:
			if re.match(r"Error\W+on.+{}".format("module_name_error"), line[0]):
				correct_file = True
				break

	assert correct_file #an error message was printed with the correct textblock name

	mark_in_code = False
	for line in mod_i.lines:
		if re.match(r"[^#]+#\W<<<.*ERROR", line.body):
			mark_in_code = True
			break

	assert mark_in_code # the main script has been marked with a comment

	mark_in_code = False
	for line in name_error_textblock.lines:
		if re.match(r"[^#]+#\W<<<\WERROR", line.body):
			mark_in_code = True
			break

	assert mark_in_code # the imported script has been marked with a comment




# ======  End of IMPORT ERRORS  =======


