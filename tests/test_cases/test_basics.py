import pytest
import addon_utils
import sys

def get_version(suzanne_codes_addon):
	mod = sys.modules[suzanne_codes_addon]
	return mod.bl_info.get("version", (-1, -1, -1))

def test_versionID_pass(suzanne_codes_addon):
	expect_version = (0, 1, 0)
	return_version = get_version(suzanne_codes_addon)
	assert expect_version == return_version

def test_blender_version(bpy_test):
	assert bpy_test.data.version >= (2, 80 ,0), "Blender version should be greater or equal to 2.80"

def test_suzanne_sc(sznn_sc):
	assert "Empty_for_test" in sznn_sc.objects

def test_empty_sc(empty_sc):
	assert len(empty_sc.objects) == 0