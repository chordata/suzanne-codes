import pytest
from random import random
import mathutils
import math


@pytest.fixture
def suzanne_codes_addon(cache):
    return cache.get("addon_name", None)


@pytest.fixture
def suzanne_codes_module():
    import suzanne_codes
    suzanne_codes.lib.state._reset_sznn_state()
    return suzanne_codes


@pytest.fixture
def sznn(suzanne_codes_module):
    return suzanne_codes_module.lib

@pytest.fixture
def sznn_ops(suzanne_codes_module):
    return suzanne_codes_module.ops

@pytest.fixture
def sznn_get_object(suzanne_codes_module):
    return suzanne_codes_module.lib.get_object

@pytest.fixture
def bpy_test():
    import bpy
    return bpy

@pytest.fixture
def bpy_context(bpy_test):
	return bpy_test.context

@pytest.fixture
def bpy_data(bpy_test):
	return bpy_test.data

@pytest.fixture
def empty_sc(sznn, bpy_test):
	assert not sznn.state._get_sznn_state("ob_cache")

	D = bpy_test.data

	for ob in D.objects:
	    D.objects.remove(ob)

	for mesh in D.meshes:
	    D.meshes.remove(mesh)

	for coll in D.collections:
	    D.collections.remove(coll)	

	yield bpy_test.context.scene

	for ob in D.objects:
	    D.objects.remove(ob)

	for mesh in D.meshes:
	    D.meshes.remove(mesh)	

	for coll in D.collections:
	    D.collections.remove(coll)	

	sznn._reset_sznn_state()

@pytest.fixture
def sznn_sc(bpy_test, empty_sc):
	D = bpy_test.data
	ob = D.objects.new("Empty_for_test", None)
	assert ob.name == "Empty_for_test", "Another object was present in a scene that was supposed to be empty"
	empty_sc.collection.objects.link(ob)
	yield empty_sc

@pytest.fixture
def sznn_ob(sznn_sc, suzanne_codes_module):
	ob = suzanne_codes_module.lib.get_object("Empty_for_test")
	yield ob

@pytest.fixture
def sznn_two_obs(bpy_test, sznn_sc, suzanne_codes_module):
	ob1 = suzanne_codes_module.lib.get_object("Empty_for_test")
	
	D = bpy_test.data
	ob = D.objects.new("Empty_for_test2", None)
	assert ob.name == "Empty_for_test2", "Another object was present in a scene that was supposed to be empty"
	sznn_sc.collection.objects.link(ob)
	
	ob2 = suzanne_codes_module.lib.get_object("Empty_for_test2")

	yield ob1, ob2


# =================================
# =           ROTATIONS           =
# =====================`============
@pytest.fixture
def zero_rot():
	return mathutils.Euler()

@pytest.fixture
def random_rot():
	x = random() * math.pi * 2
	y = random() * math.pi * 2
	z = random() * math.pi * 2
	return mathutils.Euler((x,y,z))

@pytest.fixture
def rot_ob(sznn_ob, zero_rot):
	assert sznn_ob.rotation == zero_rot, "The object under test has a rotation different than (0,0,0)"
	yield sznn_ob


# ======  End of ROTATIONS  =======


# ==================================
# =           3D Vectors           =
# ==================================

@pytest.fixture
def zero_loc():
	return mathutils.Vector()


@pytest.fixture
def random_loc():
	x = random() * 10 - 5
	y = random() * 10 - 5
	z = random() * 10 - 5
	return mathutils.Vector((x,y,z))

@pytest.fixture
def loc_ob(sznn_ob, zero_loc):
	assert sznn_ob.location == zero_loc, "The object under test has a location different than (0,0,0)"
	yield sznn_ob


@pytest.fixture
def scale_ob(sznn_ob, scale_one):
	assert sznn_ob.scale == scale_one, "The object under test has a scale different than (1,1,1)"
	yield sznn_ob

@pytest.fixture
def scale_one():
	return mathutils.Vector((1,1,1))

@pytest.fixture
def random_scale(random_loc):
	return random_loc
# ======  End of 3D Vectors  =======



# @pytest.fixture
# def chordata_nodetree(bpy_test):
#     D = bpy_test.data
#     nodetree = D.node_groups.new("Chordata_test_nodetree", 'ChordataTreeType')
#     nodetree_name = nodetree.name
#     yield nodetree
    
#     if nodetree_name in D.node_groups:
#         D.node_groups.remove(nodetree)


# @pytest.fixture
# def basic_cube(bpy_test):
#     D = bpy_test.data
#     mesh = bpy_test.data.meshes.new('Basic_Cube')
#     return bpy_test.data.objects.new('acube', mesh)


# @pytest.fixture
# def engine(chordata_module):
#     return chordata_module.ops.engine


# @pytest.fixture
# def gui(chordata_module):
#     return chordata_module.utils.gui


# @pytest.fixture
# def chordata_avatar(bpy_test):
#     bpy = bpy_test
#     bpy.ops.chordata.avatar_add()
#     bpy.data.objects['Chordata_biped'].pose.bones['l-clavicule'].chordata.capture_bone = False
#     bpy.data.objects['Chordata_biped'].pose.bones['r-clavicule'].chordata.capture_bone = False


#     yield bpy.data.objects['Chordata_biped']
#     bpy.ops.chordata.objects_rm()
#     assert 'Chordata_biped' not in bpy.data.objects

#     for tree in bpy_test.data.node_groups:
#         if tree.bl_idname == 'ChordataTreeType':
#             bpy.data.node_groups.remove(tree)

# @pytest.fixture
# def get_OP_blender_type(chordata_module):
#     return chordata_module.utils.get_OP_blender_type


# # ==========================================
# # =           COPP, PACKETS, etc           =
# # ==========================================
# @pytest.fixture
# def copp_server(chordata_module):
#     return chordata_module.copp_server.COPP_server

# @pytest.fixture
# def copp_packet(copp_server):
#     return copp_server.COPP_Common_packet

# @pytest.fixture
# def rotated_quats():
#     from mathutils import Euler, Quaternion
#     quats = [
#         Quaternion(Euler((1,0,0))),
#         Quaternion(Euler((0,1,0))),
#         Quaternion(Euler((1,0,1)))
#     ]
#     return quats

# @pytest.fixture
# def copp_Q_packet(copp_packet, copp_server, engine, rotated_quats):
#     p = copp_packet()
#     p.target = engine.DataTarget.Q
#     addrpattern = "/Chordata/q/head"
#     typetags = ",ffff"
#     msg = copp_server.COPP_Message(addrpattern, typetags, tuple(rotated_quats[0]))
#     p._elements.append(msg)
#     addrpattern = "/Chordata/q/dorsal"
#     msg = copp_server.COPP_Message(addrpattern, typetags, tuple(rotated_quats[1]))
#     p._elements.append(msg)
#     addrpattern = "/Chordata/q/base"
#     msg = copp_server.COPP_Message(addrpattern, typetags, tuple(rotated_quats[2]))
#     p._elements.append(msg)
#     assert p.target == engine.DataTarget.Q
#     assert p._get_elements()[0].subtarget == "head"
#     assert p._get_elements()[1].subtarget == "dorsal"
#     assert p._get_elements()[2].subtarget == "base"
#     return p


# @pytest.fixture
# def copp_ROT_packet(copp_packet, copp_server, engine, rotated_quats):
#     p = copp_packet()
#     p.target = engine.DataTarget.ROT
#     addrpattern = "/Chordata/rot/head"
#     typetags = ",ffff"
#     msg = copp_server.COPP_Message(addrpattern, typetags, tuple(rotated_quats[0]))
#     p._elements.append(msg)
#     addrpattern = "/Chordata/rot/dorsal"
#     msg = copp_server.COPP_Message(addrpattern, typetags, tuple(rotated_quats[1]))
#     p._elements.append(msg)
#     addrpattern = "/Chordata/rot/base"
#     msg = copp_server.COPP_Message(addrpattern, typetags, tuple(rotated_quats[2]))
#     p._elements.append(msg)
#     assert p.target == engine.DataTarget.ROT
#     assert p._get_elements()[0].subtarget == "head"
#     assert p._get_elements()[1].subtarget == "dorsal"
#     assert p._get_elements()[2].subtarget == "base"
#     return p


# @pytest.fixture
# def copp_COMM_packet(copp_packet, copp_server, engine):
#     p = copp_packet()
#     p.target = engine.DataTarget.COMM
#     addrpattern = "/Chordata/comm"
#     typetags = ",s"
#     payload = "Test communication packet"
#     msg = copp_server.COPP_Message(addrpattern, typetags, payload)
#     p._elements.append(msg)
#     assert p.target == engine.DataTarget.COMM
#     assert p._get_elements()[0].subtarget is None
#     return p

# @pytest.fixture
# def copp_RAW_packet(copp_packet, copp_server, engine):
#     p = copp_packet()
#     p.target = engine.DataTarget.RAW
#     addrpattern = "/Chordata/raw/raw_head"
#     typetags = ",iiiiiiiii"
#     msg = copp_server.COPP_Message(addrpattern, typetags, (1,2,3)*3)
#     p._elements.append(msg)
#     addrpattern = "/Chordata/raw/raw_dorsal"
#     msg = copp_server.COPP_Message(addrpattern, typetags, (1,2,3)*3)
#     p._elements.append(msg)
#     addrpattern = "/Chordata/raw/raw_base"
#     msg = copp_server.COPP_Message(addrpattern, typetags, (1,2,3)*3)
#     p._elements.append(msg)
#     return p

# # ======  End of COPP, PACKETS, etc  =======

# # =======================================
# # =           ENGINE FIXTURES           =
# # =======================================

# @pytest.fixture
# def engine_node(chordata_nodetree, engine):
#     node_1 = engine.EngineNode(chordata_nodetree.nodes.new('DumpNodeType'))
#     return node_1

# @pytest.fixture
# def TestNodeClass(engine):
#     counter = {}
#     # function wrapper for counting calls
#     def wrap_count(func):
#         def call_and_count(*args, packet):
#             nonlocal counter
#             if args[0].name not in counter.keys():
#                 counter[args[0].name] = {"Q_handler": 0, "ERR_handler": 0, "ROT_handler": 0}

#             counter[args[0].name][func.__name__] += 1
#             return func(*args, packet=packet)
#         return call_and_count

#     # derived testing engine node class with counter handler wrapping
#     class TestNode(engine.EngineNode):
#         @wrap_count
#         def Q_handler(self, packet):
#             self.q_msgs.append(packet)
#             # Input Q, No Output
#             print(self.name + " received: ", packet)

#         @wrap_count
#         def ERR_handler(self, packet):
#             # Input ERR, Output ERR. Trimmed string
#             packet.payload = packet.payload[:-1] 
#             return packet

#         @wrap_count
#         def ROT_handler(self, packet):
#             # Input ROT, Output Q
#             output_packet = packet.payload[:-2]


#         def __init__(self, name, _engine = None, _fetch_by_name = False): 
#             super().__init__(name, _engine, _fetch_by_name)
#             self.q_msgs = []
#             self.exposed_handlers[engine.DataTarget.Q] = self.Q_handler
#             self.exposed_handlers[engine.DataTarget.ERR] = self.ERR_handler
#             self.exposed_handlers[engine.DataTarget.ROT] = self.ROT_handler

#     return TestNode, counter

# @pytest.fixture
# def test_node_tree(bpy_test, TestNodeClass, engine_node, chordata_nodetree, engine, copp_Q_packet):
#     node_1 = engine_node
#     TestNode, counter = TestNodeClass

#     node_1 = TestNode(chordata_nodetree.nodes.new('DumpNodeType'))
#     node_2 = TestNode(chordata_nodetree.nodes.new('DumpNodeType'))
#     node_3 = TestNode(chordata_nodetree.nodes.new('DumpNodeType'))

#     assert node_1.children == []
#     assert node_2.children == []
#     assert node_3.children == []

#     node_1.connect(node_2)
#     node_2.connect(node_3)

#     return {"nodes": (node_1, node_2, node_3), "counter": counter}


# @pytest.fixture
# def fake_chordata_node(chordata_module, bpy_test, engine ):
#     from bpy.utils import register_class, unregister_class

#     class FakeChordataNode(chordata_module.nodes.basenode.ChordataBaseNode):
#         bl_idname = 'FakeChordataNodeType'
#         bl_label = "Fake Chordata Node (for testing only)"

#         @engine.datatarget_handler(engine.DataTarget.Q)
#         @engine.datatarget_handler(engine.DataTarget.ROT)
#         def some_handler(self, packet=None):
#             return "ROT & Q handler response"

#         @engine.helper_method
#         def fake_fn(self, arg1="NO ARG", arg2="NO ARG"):
#             return (self, arg1, arg2)

#         def raw_handler(self):
#             return "RAW handler response"

#         def init(self, context):
#             self.inputs.new('DataStreamSocketType', "capture_in")

#     register_class(FakeChordataNode)
#     yield FakeChordataNode.bl_idname
#     unregister_class(FakeChordataNode)

# @pytest.fixture
# def fake_regular_node(chordata_module, bpy_test, engine ):
#     from bpy.utils import register_class, unregister_class

#     class FakeRegularNode(bpy_test.types.Node):
#         bl_idname = 'FakeRegularNodeType'
#         bl_label = "Fake Regular Node (for testing only)"

#         @engine.datatarget_handler(engine.DataTarget.ROT)
#         def some_handler(self, packet=None):
#             return "ROT handler response"

#         @engine.helper_method
#         def fake_fn(self, arg1="NO ARG", arg2="NO ARG"):
#             return (self, arg1, arg2)

#         def init(self, context):
#             self.inputs.new('DataStreamSocketType', "capture_in")

#     register_class(FakeRegularNode)
#     yield FakeRegularNode.bl_idname
#     unregister_class(FakeRegularNode)


# @pytest.fixture
# def base_fake_nodetree(chordata_nodetree, fake_chordata_node):
#     t_node = chordata_nodetree.nodes.new('TestCubeNodeType')
#     n_node = chordata_nodetree.nodes.new('NotochordNodeType')
#     f_node = chordata_nodetree.nodes.new(fake_chordata_node)

#     socket_in = t_node.inputs[0]
#     socket_out = n_node.outputs[0]
#     chordata_nodetree.links.new(socket_in, socket_out)

#     socket_in = f_node.inputs[0]
#     socket_out = t_node.outputs[0]
#     chordata_nodetree.links.new(socket_in, socket_out)

#     assert n_node.outputs[0].is_linked
#     assert t_node.inputs[0].is_linked
#     assert t_node.outputs[0].is_linked
#     assert f_node.inputs[0].is_linked

#     return (t_node, n_node, f_node)


# @pytest.fixture
# def vector_testcube_nodetree(chordata_nodetree, fake_chordata_node):
#     t_node = chordata_nodetree.nodes.new('TestCubeNodeType')
#     n_node = chordata_nodetree.nodes.new('NotochordNodeType')
#     v_node = chordata_nodetree.nodes.new('VectorNodeType')

#     socket_in = t_node.inputs[0]
#     socket_out = n_node.outputs[0]
#     chordata_nodetree.links.new(socket_in, socket_out)

#     socket_in = v_node.inputs[0]
#     socket_out = n_node.outputs[0]
#     chordata_nodetree.links.new(socket_in, socket_out)

#     assert n_node.outputs[0].is_linked
#     assert t_node.inputs[0].is_linked
#     assert v_node.inputs[0].is_linked

#     return (t_node, n_node, v_node)


# # ======  End of ENGINE FIXTURES  =======