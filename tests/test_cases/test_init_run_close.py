import pytest


def test_init_state(sznn):
	sznn.state._set_sznn_state("foo", 123)
	sznn.state._modal = "BAR"
	sznn.state._timekeeper = "FAS"
	assert sznn.state._get_sznn_state("stage") == sznn.state.SuzanneCodes_STAGES.IDLE
	sznn.__suzanne_code_init__()
	assert sznn.state._get_sznn_state("stage") == sznn.state.SuzanneCodes_STAGES.SETUP
	assert sznn.state._modal is None
	assert sznn.state._timekeeper is None


def test_run_timekeeper(sznn, mocker):
	sznn.__suzanne_code_run__({})
	assert sznn.state._get_sznn_state("stage") == sznn.state.SuzanneCodes_STAGES.RUN
	assert type(sznn.state._timekeeper) == sznn.utils.Timekeeper
	
	sznn.state._timekeeper.ellapsed_time = mocker.Mock(return_value = 5.23467)

	assert sznn.millis() == int(sznn.state._timekeeper.ellapsed_time()*1000)
	assert sznn.seconds() == sznn.state._timekeeper.ellapsed_time()
	assert sznn.seconds() == 5.23467
	sznn.__suzanne_code_close__()
	assert sznn.state._timekeeper is None
	

def test_set_fps(bpy_test, sznn):
	sznn.__suzanne_code_init__()
	sznn.set_fps(29)
	assert sznn.get_fps() == 29

	sznn.__suzanne_code_run__({})
	with pytest.raises(RuntimeError):
		sznn.set_fps(34)
		
	assert bpy_test.context.scene.render.fps == 29
