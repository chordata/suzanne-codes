import math
import pytest
import time


# ========================================
# =           ANGLE CONVERSION           =
# ========================================

def test_deg_to_rad(sznn):
	a = 90
	res = sznn.deg_to_rad(a)
	assert math.pi/2 == res

	b = 360 - 45
	res = sznn.deg_to_rad(b)
	assert math.pi + math.pi * 3/4  == res

	#test iterable conversion
	res = sznn.deg_to_rad((a, b, 0, 360))
	assert res[0] == math.pi/2
	assert res[1] == math.pi + math.pi * 3/4
	assert res[2] == 0
	assert res[3] == math.pi * 2

	assert len(res) == 4

def test_deg_to_rad_wrong(sznn):
	with pytest.raises(TypeError):
		sznn.deg_to_rad("90")

	with pytest.raises(TypeError):
		sznn.deg_to_rad((10, 5, "90"))


def test_rad_to_deg(sznn):
	a = math.pi/2 
	res = sznn.rad_to_deg(a)
	assert 90 == res

	b = math.pi + math.pi * 3/4
	res = sznn.rad_to_deg(b)
	assert  360 - 45 == res

	#test iterable conversion
	res = sznn.rad_to_deg((a, b, 0, math.pi * 2))
	assert res[0] == 90
	assert res[1] == 360 - 45
	assert res[2] == 0
	assert res[3] == 360

	assert len(res) == 4

def test_rad_to_deg_wrong(sznn):
	with pytest.raises(TypeError):
		sznn.rad_to_deg("3.1419")

	with pytest.raises(TypeError):
		sznn.rad_to_deg((math.pi, math.pi/2, "6.28"))

# ======  End of ANGLE CONVERSION  =======


@pytest.fixture
def timekeeper(sznn):
	return sznn.utils.Timekeeper()

def test_timer_basic(timekeeper):
	rate = 100 #in Hz
	for i in range(timekeeper.max_queue_len+ 2):
		timekeeper.lap()
		time.sleep(1/rate)

	assert len(timekeeper.hist) == timekeeper.max_queue_len
	assert timekeeper.avg_lap() == pytest.approx(1/rate, abs=0.001)
	assert timekeeper.get_last_lap() == pytest.approx(1/rate, abs=0.001)
	assert timekeeper.get_rate() == pytest.approx(rate, abs=4)