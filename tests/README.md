# Test Suite for SuzanneCodes Addon

[![pipeline status](https://gitlab.com/chordata/suzanne-codes/badges/master/pipeline.svg)](https://gitlab.com/chordata/suzanne-codes/pipelines) [![coverage report](https://gitlab.com/chordata/suzanne-codes/badges/master/coverage.svg)](https://gitlab.com/chordata/suzanne-codes/-/jobs/artifacts/master/file/tests/htmlcov/index.html?job=coverage)

### Run the tests:

<div style="padding:16px; background-color: #ececb2;border-radius: 6px;"><strong>Attention!</strong><p>Running the tests when the addon is activated in the saved blender preferences will result in a smaller coverage. Deactivate the addon in blender and save the preferences to get the real values.</p></div>

```bash
blender -b --python run_pytest.py [-- <extra args to be passed to pytest>]
```

Note that you can pass arguments to pytest after a `--` separator. For example to make it fail at the first error use:

```bash
blender -b --python run_pytest.py -- -x
```

### Get the coverage report:
the html version of the coverage report can be obtained by running:

```bash
blender -b --python run_pytest.py -- --cov --cov-report=html
```
a plain console dump can be obtained by skipping the `--cov-report` flag.

## Installation

Before starting you should install `pytest` in a location reachable to the python interpreter bundled with Blender.

### Install pytest (linux):

(probably works on mac as well, not tested yet)

```bash
./setup_pytest.py
```

### Install pytest (windows):

NOTE:
Adding Blender executable folder to the PATH environment variable will make it possible to call blender.exe just by typing "blender" in the console rather then the whole path.
If you decide not to do it you must type the whole path whenever "blender" command is used in this guide.

(CMD must be runned as Administrator for most of these steps to work)

1. Locate the Python interpreter bundled with Blender by running in CMD:

```
blender -b
```

It should output (among other things):

```
found bundled python: C:\Program Files\Blender Foundation\Blender\2.80\python
```

2. Set current path in CMD to the python interpreter folder:

```
cd <path found in step 1>\bin
```

3. From here, to install pip for this python interpreter, run:

```
python.exe -m ensurepip --default-pip
```

4. Upgrade pip:

```
python.exe -m pip install --upgrade pip
```

or https://stackoverflow.com/questions/53764054/python-3-7-pip-upgrade-error-on-windows-10

5. Install pytest:

```
python.exe -m pip install pytest
```

6. Install pytest-mock:
```
python.exe -m pip install pytest-mock
```

## Credits

Pytest loading in blender based upon Douglas Kastle's [blender-fake-addon](https://github.com/douglaskastle/blender-fake-addon). Thanks Douglas for sharing it!
[How to install pip for a specific python version](https://stackoverflow.com/questions/42371406/how-to-install-pip-for-a-specific-python-version)
