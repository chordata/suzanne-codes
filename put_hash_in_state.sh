#!/bin/bash


CURRENT_HASH=$CI_COMMIT_SHORT_SHA

if [ -n "$CURRENT_HASH" ]; then
	CURRENT_HASH="'#$CURRENT_HASH'"
fi

if [ -z "$CURRENT_HASH" ]; then
	CURRENT_HASH="'dev#$(git rev-parse --short HEAD)'"
fi

if [ -z "$CURRENT_HASH" ]; then
	CURRENT_HASH="'dev'"
fi


sed -i -e "s/GIT_HASH *=.*/GIT_HASH = $CURRENT_HASH/g" addons/suzanne_codes/lib/state.py